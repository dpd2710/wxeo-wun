<?php
/**
 * WXEO Wun functions and definitions
 *
 * @package WXEO Wun
 */


define('WP_WXEO',       dirname( __FILE__ ) . '/wp-wxeo/');
define('WP_WXEO_ASSET', get_template_directory_uri());



/**
 * PHP Dependencies
 ******************************************/

// require WP_WXEO . 'TEMPLATE.php';             // TEMPLATE

// Frontnd
require WP_WXEO . 'nav.php';                      // Custom Navigation
require WP_WXEO . 'template-tags.php';            // Custom template tags for this theme.
require WP_WXEO . 'extras.php';                   // Custom functions that act independently of the theme templates.
require WP_WXEO . 'customizer.php';               // Customizer additions.
require WP_WXEO . 'jetpack.php';                  // Load Jetpack compatibility file.
require WP_WXEO . 'search.php';                   // Custom Search Widget
require WP_WXEO . 'portfolio.php';                // Portfolio


// Admin
require WP_WXEO . 'tgm.php';                      // Theme Plugin Activation
require WP_WXEO . '/admin/meta-post-formats.php'; // Post Format Metabox



require WP_WXEO . '/assets/shortcodes/wxeo-button.php';

// shortcodes
if ( function_exists( 'vc_map' ) ) {
  
  if (wxeo_wun('wxeo-shortcode-feature-list-on-off'))           { require WP_WXEO . '/assets/shortcodes/feature-list.php';          }
  if (wxeo_wun('wxeo-shortcode-feature-inline-list-on-off'))    { require WP_WXEO . '/assets/shortcodes/feature-list-inline.php';   }
  if (wxeo_wun('wxeo-shortcode-feature-box-list-on-off'))       { require WP_WXEO . '/assets/shortcodes/feature-list-box.php';      }
  if (wxeo_wun('wxeo-shortcode-feature-centered-list-on-off'))  { require WP_WXEO . '/assets/shortcodes/feature-list-centered.php'; }
  //if (wxeo_wun('wxeo-button'))  { require WP_WXEO . '/assets/shortcodes/wxeo-button.php'; }
  require WP_WXEO . '/assets/shortcodes/milestone-counter.php';
  
  
}


if ( !class_exists( 'ReduxFramework' ) && file_exists( WP_WXEO . 'cyclone/framework.php' ) ) {
    require_once( WP_WXEO . 'cyclone/framework.php' );
}
if ( !isset( $redux_demo ) && file_exists( WP_WXEO . 'cyclone-assets/config.php' ) ) {
    require_once( WP_WXEO . 'cyclone-assets/config.php' );
    // require_once( WP_WXEO . 'cyclone-assets/config.sample.php' );
}




/**
 * JS Dependencies
 ******************************************/

function wxeo_scripts() {
  
  // CSS
  wp_enqueue_style( 'wxeo-style', get_stylesheet_uri() );

  wp_enqueue_style( 'wxeo-cyclone-style', WP_WXEO_ASSET . '/wp-wxeo/cyclone-assets/style.min.css' );                                       // Cyclone CSS Output



  // JS
  wp_enqueue_script( 'wxeo-navigation',           WP_WXEO_ASSET . '/js/navigation.js', array('jquery'), '20120206', true );                // Responsive Navigation

  wp_enqueue_script( 'wxeo-skip-link-focus-fix',  WP_WXEO_ASSET . '/js/skip-link-focus-fix.js', array('jquery'), '20130115', true );

  wp_enqueue_script( 'wxeo-cyclone-js',           WP_WXEO_ASSET . '/wp-wxeo/cyclone-assets/main.min.js', array('jquery'), false, true );   // Cyclone JS Output

  wp_enqueue_script( 'wxeo-js',                 WP_WXEO_ASSET . '/js/main.js', array('jquery'), '3.1.1', true );                         // Custom JS

  wp_enqueue_script( 'bootstrap',                 WP_WXEO_ASSET . '/js/bootstrap.min.js', array('jquery'), '3.1.1', true );                // Bootstrap JS

  wp_enqueue_script( 'responsive-slides',         WP_WXEO_ASSET . '/js/ResponsiveSlides.min.js', array('jquery'), '4.1.2', true );         // Blog Post Format Gallery Slider


  if ( is_singular() && comments_open() && get_option( 'thread_comments' ) ) {
    wp_enqueue_script( 'comment-reply' );
  }

} add_action( 'wp_enqueue_scripts', 'wxeo_scripts' );









// Options Panel Info
function wxeo_wun($extend=false) {
    global $wxeo_wun;
    if ($extend) {
      return $wxeo_wun[$extend];
    }
    return $wxeo_wun;
}


// Disable Update Notifier for Visual Composer.
if ( function_exists( 'vc_set_as_theme' ) ) {
  vc_set_as_theme($notifier = false);
}






/**
 * Set the content width based on the theme's design and stylesheet.
 */
if ( ! isset( $content_width ) ) {
	$content_width = 640; /* pixels */
}

if ( ! function_exists( 'wxeo_setup' ) ) {

  /**
   * Sets up theme defaults and registers support for various WordPress features.
   *
   * Note that this function is hooked into the after_setup_theme hook, which
   * runs before the init hook. The init hook is too late for some features, such
   * as indicating support for post thumbnails.
   */
  function wxeo_setup() {

  	
  	/*
  	 * Make theme available for translation.
  	 * Translations can be filed in the /languages/ directory.
  	 * If you're building a theme based on WXEO Wun, use a find and replace
  	 * to change 'wxeo' to the name of your theme in all the template files
  	 */
  	load_theme_textdomain( 'wxeo', get_template_directory() . '/languages' );

  	// Add default posts and comments RSS feed links to head.
  	add_theme_support( 'automatic-feed-links' );

  	/*
  	 * Enable support for Post Thumbnails on posts and pages.
  	 *
  	 * @link http://codex.wordpress.org/Function_Reference/add_theme_support#Post_Thumbnails
  	 */
  	//add_theme_support( 'post-thumbnails' );

  	// This theme uses wp_nav_menu() in one location.
  	register_nav_menus( array(
  		'primary' => __( 'Primary Menu', 'wxeo' )
  	) );

  	// Enable support for Post Formats.
  	add_theme_support( 'post-formats', array( 'gallery', 'audio', 'video', 'quote', 'link' ) );

  	// Setup the WordPress core custom background feature.
  	add_theme_support( 'custom-background', apply_filters( 'wxeo_custom_background_args', array(
  		'default-color' => 'ffffff',
  		'default-image' => '',
  	) ) );

  	// Enable support for HTML5 markup.
  	add_theme_support( 'html5', array(
  		'comment-list',
  		'search-form',
  		'comment-form',
  		'gallery',
  		'caption',
  	) );
  }

} add_action( 'after_setup_theme', 'wxeo_setup' );

/**
 * Register widget area.
 *
 * @link http://codex.wordpress.org/Function_Reference/register_sidebar
 */
function wxeo_widgets_init() {

	register_sidebar( array(
		'name'          => __( 'Sidebar', 'wxeo' ),
		'id'            => 'sidebar-1',
		'description'   => '',
		'before_widget' => '<aside id="%1$s" class="widget %2$s">',
		'after_widget'  => '</aside>',
		'before_title'  => '<h1 class="widget-title">',
		'after_title'   => '</h1>',
	) );

} add_action( 'widgets_init', 'wxeo_widgets_init' );



/* Find each available shortcode in a page or post */
function find_shortcodes($atts) {
  ob_start();
  
  $string = "[";
  $string .= $atts;

    $args = array('s' => $string);
    $the_query = new WP_Query( $args );
      if ( $the_query->have_posts() ) {
        echo true;
      } else {
        echo false;
      }
  wp_reset_postdata();
  return ob_get_clean();
}


// if (find_shortcodes('feature-list-box')) {
//   echo "something worked!";
// } else {
//   echo "nothing";
// }


function new_excerpt_length($length) {

  return 50;

} add_filter('excerpt_length', 'new_excerpt_length');



function new_excerpt_more( $more ) {
  
  return '... <br> <a class="read-more btn btn-default" href="'. get_permalink( get_the_ID() ) . '">' . __('Read More', 'your-text-domain') . '</a>';

} add_filter( 'excerpt_more', 'new_excerpt_more' );








// Add  theme Support for Thumbnail only in portfolio Post Type
add_theme_support('post-thumbnails');


// Image sizes
add_image_size('blog-image-large-crop', 1140, 380, true);
add_image_size('blog-image-large-crop-sm', 1140, 180, true);
add_image_size('blog-image-large-crop-lg', 1140, 580, true);
add_image_size('blog-image-large', 1140, 380);


add_image_size('blog-image-medium-crop', 853, 380, true);
add_image_size('blog-image-medium-crop-sm', 853, 180, true);
add_image_size('blog-image-medium-crop-lg', 853, 580, true);
add_image_size('blog-image-medium', 853, 380);


function blog_image_size($type="blog") {


  if (wxeo_wun('wxeo-blog-style') == 'wide' || wxeo_wun('wxeo-blog-style') == 'wide-center') {
    $output = array();
    $output['sidebar'] = $output['height'] = ''; 

    if ($type == 'single') {
      $output['sidebar'] = '-single';
      $type_height = 'wxeo-blog-single-image-height';
    } else {
      $type_height = 'wxeo-blog-image-height';
    }

    switch (wxeo_wun($type_height)) {
      case 'small':
        $output['height'] = '-crop-sm';
        break;
      case 'medium':
        $output['height'] = '-crop';
        break;
      case 'large':
        $output['height'] = '-crop-lg';
        break;
    }

    if (wxeo_wun('wxeo-blog'.$output['sidebar'].'-sidebar')) {
      return 'blog-image-medium'.$output['height'];
    } else {
      return 'blog-image-large'.$output['height'];
    }
    
  }

  return 'thumbnail';

}



// // add shortcode for Visual COmposer

// vc_map( array(
//     "name" => __("Bar tag test"),
//     "base" => "bartag",
//     "class" => "",
//     "category" => __('Content'),
//     'admin_enqueue_js' => array(get_template_directory_uri().'/vc_extend/bartag.js'),
//     'admin_enqueue_css' => array(get_template_directory_uri().'/vc_extend/bartag.css'),
//     "params" => array(
//         array(
//             "type" => "textfield",
//             "holder" => "div",
//             "class" => "",
//             "heading" => __("Text"),
//             "param_name" => "foo",
//             "value" => __("Default params value"),
//             "description" => __("Description for foo param.")
//         ),
//         array(
//             "type" => "colorpicker",
//             "holder" => "div",
//             "class" => "",
//             "heading" => __("Text color"),
//             "param_name" => "color",
//             "value" => '#FF0000', //Default Red color
//             "description" => __("Choose text color")
//         ),
//       array(
//             "type" => "my_param",
//             "holder" => "div",
//             "class" => "",
//             "heading" => __("Flipping text", "js_composer"),
//             "param_name" => "color",
//             "value" => '',
//             "description" => __("Enter text and flip it", "js_composer")
//         )
//     )
// ) );





// Ensure cart contents update when products are added to the cart via AJAX (place the following in functions.php)
add_filter('add_to_cart_fragments', 'woocommerce_header_add_to_cart_fragment');
 
function woocommerce_header_add_to_cart_fragment( $fragments ) {
  global $woocommerce;
  
  ob_start();
  
  ?>
  <a class="cart-contents" href="<?php echo $woocommerce->cart->get_cart_url(); ?>" title="<?php _e('View your shopping cart', 'woothemes'); ?>"><?php echo sprintf(_n('%d item', '%d items', $woocommerce->cart->cart_contents_count, 'woothemes'), $woocommerce->cart->cart_contents_count);?> - <?php echo $woocommerce->cart->get_cart_total(); ?></a>
  <?php
  
  $fragments['a.cart-contents'] = ob_get_clean();
  
  return $fragments;
  
}