<?php
/**
 * The template for displaying Comments.
 *
 * The area of the page that contains both current comments
 * and the comment form.
 *
 * @package WXEO Wun
 */

/*
 * If the current post is protected by a password and
 * the visitor has not yet entered the password we will
 * return early without loading the comments.
 */
if ( post_password_required() ) {
	return;
}
?>

<?php
function my_custom_comments($comment, $args, $depth) {
   $GLOBALS['comment'] = $comment; ?>
   <li <?php comment_class('media'); ?> id="comment-<?php comment_ID() ?>">
   <?php if ($comment->comment_approved == '0') : ?>
      <em><?php _e('Your comment is awaiting moderation.') ?></em>
   <?php endif; ?>
  
    <a class="pull-left" href="#">
    	<?php echo get_avatar($comment,$size='64'); ?>
    </a>
    <div class="media-body">
      <h4 class="media-heading"><a href="<?php comment_author_url(); ?>" target="blank"><?php comment_author(); ?></a></h4>

      <div class="comment-meta commentmetadata">
	   		<a href="<?php echo htmlspecialchars( get_comment_link( $comment->comment_ID ) ) ?>">
	   			<?php printf(__('%1$s at %2$s'), get_comment_date(),  get_comment_time()) ?>
	   		</a>
	   		<?php edit_comment_link(__('(Edit)'),'  ','') ?>
	   	</div>

      <?php comment_text(); ?>
      <div class="reply"><?php comment_reply_link(array_merge( $args, array('depth' => $depth, 'max_depth' => $args['max_depth']))) ?></div>
    </div>

  </li>


   <?php
}
?>

<?php if ( have_comments() ) : ?>
<ul class="media-list">
    <?php wp_list_comments("callback=my_custom_comments"); ?>
</ul>
<?php endif; // have_comments() ?>

<?php
	// If comments are closed and there are comments, let's leave a little note, shall we?
	if ( ! comments_open() && '0' != get_comments_number() && post_type_supports( get_post_type(), 'comments' ) ) :
?>
	<p class="no-comments"><?php _e( 'Comments are closed.', 'wxeo' ); ?></p>
<?php endif; ?>




<?php if ('open' == $post->comment_status) : ?>


<div id="respond">
 
<h3><?php comment_form_title( 'Leave a Reply', 'Leave a Reply to %s' ); ?></h3>
 
<div class="cancel-comment-reply">
    <small><?php cancel_comment_reply_link(); ?></small>
</div>
 
<?php if ( get_option('comment_registration') && !$user_ID ) : ?>
<p>You must be <a href="<?php echo get_option('siteurl'); ?>/wp-login.php?redirect_to=<?php echo urlencode(get_permalink()); ?>">logged in</a> to post a comment.</p>
<?php else : ?>
 
<form action="<?php echo get_option('siteurl'); ?>/wp-comments-post.php" method="post" id="commentform">
 
<?php if ( $user_ID ) : ?>
	Logged in as <a href="<?php echo get_option('siteurl'); ?>/wp-admin/profile.php"><?php echo $user_identity; ?></a>. <a href="<?php echo wp_logout_url(get_permalink()); ?>" title="Log out of this account">Log out &raquo;</a>
<?php else : ?>
	<div class="row">
		<div class="col-md-6">
			<label for="author">Name <?php if ($req) echo "(required)"; ?></label>
			<input class="form-control" type="text" name="author" id="author" value="<?php echo $comment_author; ?>" size="22" tabindex="1" />
		</div>
		<div class="col-md-6">
			<label for="email">Email (will not be published) <?php if ($req) echo "(required)"; ?></label>
			<input class="form-control" type="text" name="email" id="email" value="<?php echo $comment_author_email; ?>" size="22" tabindex="2" />
		</div>
	</div>
	<label for="url">Website</label>
	<input class="form-control" type="text" name="url" id="url" value="<?php echo $comment_author_url; ?>" size="22" tabindex="3" />
<?php endif; ?>

<label for="comment">Name <?php if ($req) echo "(required)"; ?></label>
<textarea class="form-control" name="comment" id="comment" cols="100%" rows="6" tabindex="4"></textarea>

<label for="submit"><small><strong>HTML:</strong> You can use these tags:</label>
<pre><?php echo allowed_tags(); ?></pre>

<input class="btn" name="submit" type="submit" id="submit" tabindex="5" value="Submit Comment" />
<?php comment_id_fields(); ?>

<?php do_action('comment_form', $post->ID); ?>
 
</form>
 
<?php endif; // If registration required and not logged in ?>
</div>
<?php endif; // if you delete this the sky will fall on your head ?>