<?php
/**
 * @package WXEO Wun
 */
?>


<?php if (wxeo_wun('wxeo-blog-style') === 'thumbnail') : ?>



<div class="media">
  <a class="pull-left" href="#">
    <img class="media-object" src="..." alt="...">
  </a>
  <div class="media-body">
    <h4 class="media-heading">Media heading</h4>
    ...
  </div>
</div>

<?php else: ?>





<article id="post-<?php the_ID(); ?>" <?php post_class( array('blog') ); ?>>
	<header class="entry-header">
		
		<?php
			if(!get_post_format()) {
				get_template_part('wp-wxeo/assets/formats/format', 'standard');
			} else {
				get_template_part('wp-wxeo/assets/formats/format', get_post_format());
			}
		?>



		<div class="blog-article-content">

			<?php the_title( sprintf( '<h2 class="entry-title"><a href="%s" rel="bookmark">', esc_url( get_permalink() ) ), '</a></h2>' ); ?>
			
			<?php if ( 'post' == get_post_type() ) : ?>
			<div class="entry-meta">
				<?php
				
				wxeo_posted_on();

				/* translators: used between list items, there is a space after the comma */
				$categories_list = get_the_category_list( __( ', ', 'wxeo' ) );
				if ( $categories_list && wxeo_categorized_blog() ) {
					echo '<span class="post-meta-devide"> | </span>';
					printf( __( '%1$s', 'wxeo' ), $categories_list );
				}


				/* translators: used between list items, there is a space after the comma */
				$tags_list = get_the_tag_list( '', __( ', ', 'wxeo' ) );
					
				if ( $tags_list ) {
					echo '<span class="post-meta-devide"> | </span>';
					printf( __( 'Tag: %1$s', 'wxeo' ), $tags_list );
			 	}

			 	?>
			</div><!-- .entry-meta -->
			<?php endif; ?>

		</div> <!-- /.blog-article-content -->


	</header><!-- .entry-header -->

	<?php if ( is_search() ) : // Only display Excerpts for Search ?>
	<div class="entry-summary">
		<?php the_excerpt(); ?>
	</div><!-- .entry-summary -->
	<?php else : ?>
	<div class="entry-content">
		<?php the_excerpt( __( 'Continue reading <span class="meta-nav">&rarr;</span>', 'wxeo' ) ); ?>

		<?php
			wp_link_pages( array(
				'before' => '<div class="page-links">' . __( 'Pages:', 'wxeo' ),
				'after'  => '</div>',
			) );
		?>
	</div><!-- .entry-content -->
	<?php endif; ?>



</article><!-- #post-## -->


<?php endif; ?>