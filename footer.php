</div>

<div class="clearfix"></div>

<footer class="site-footer" id="site-footer-widgets" role="contentinfo">
	<div class="container">
<?php if(wxeo_wun('wxeo-footer-style') == 'one-column') { ?>
		<div class="row">
			<?php if ( !function_exists('dynamic_sidebar') || !dynamic_sidebar('one-column-footer') ) : ?><?php endif; ?>	
		</div>
<?php } ?>
<?php if(wxeo_wun('wxeo-footer-style') == 'two-column') { ?>
		<div class="row">
			<?php if ( !function_exists('dynamic_sidebar') || !dynamic_sidebar('two-column-footer') ) : ?><?php endif; ?>
		</div>
<?php } ?>
<?php if(wxeo_wun('wxeo-footer-style') == 'three-column') { ?>
		
		<div class="row">
			<?php if ( !function_exists('dynamic_sidebar') || !dynamic_sidebar('three-column-footer') ) : ?><?php endif; ?>
		</div>
<?php } ?>
<?php if(wxeo_wun('wxeo-footer-style') == 'four-column') { ?>
		<div class="row">
		<?php if ( !function_exists('dynamic_sidebar') || !dynamic_sidebar('four-column-footer') ) : ?><?php endif; ?>

		</div>
<?php } ?>
		


		

		
	</div><!-- .container -->
</footer><!-- #colophon -->

<footer class="site-footer" id="site-footer-small">
	<div class="container">

		<div class="row">
			<div class="col-md-12">
				<a href="<?php echo esc_url( __( 'http://wordpress.org/', 'wxeo' ) ); ?>"><?php printf( __( 'Proudly powered by %s', 'wxeo' ), 'WordPress' ); ?></a>
				<span class="sep"> | </span>
				<?php printf( __( 'Theme: %1$s by %2$s.', 'wxeo' ), 'WXEO Wun', '<a href="http://reduxframework.com/" rel="designer">WXEO</a>' ); ?>
			</div>			
		</div>

	</div><!-- .container -->
</footer><!-- #colophon -->





<?php wp_footer(); ?>






<script type="text/javascript">
jQuery(document).ready(function($) {
           
	$(".rslides").responsiveSlides({
	  nav: true,
	});

  

});
</script>



</body>
</html>
