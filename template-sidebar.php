<?php
/*
Template Name: Sidebar
*/

get_header(); ?>

	<div class="container asidebar">
			<div class="row">
			  <div class="col-md-9">

					<main id="main" class="site-main" role="main">

						<?php while ( have_posts() ) : the_post(); ?>

							
							
							<article id="post-<?php the_ID(); ?>" <?php post_class(); ?>>
								<header class="entry-header">
									<!-- <?php the_title( '<h1 class="entry-title">', '</h1>' ); ?> -->
								</header><!-- .entry-header -->

								<div class="entry-content">
									<?php the_content(); ?>
									<?php
										wp_link_pages( array(
											'before' => '<div class="page-links">' . __( 'Pages:', 'wxeo' ),
											'after'  => '</div>',
										) );
									?>
								</div><!-- .entry-content -->
								<footer class="entry-footer">
									<?php edit_post_link( __( 'Edit', 'wxeo' ), '<span class="edit-link">', '</span>' ); ?>
								</footer><!-- .entry-footer -->
							</article>


							<?php
								// // If comments are open or we have at least one comment, load up the comment template
								// if ( comments_open() || '0' != get_comments_number() ) :
								// 	comments_template();
								// endif;
							?>

						<?php endwhile; // end of the loop. ?>

					</main>

				</div>
			  <div class="col-md-3"><?php get_sidebar(); ?></div>
			</div>

	</div><!-- .container -->


<?php get_footer(); ?>
