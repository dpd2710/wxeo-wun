<?php
/**
 * WXEO Wun functions and definitions
 *
 * @package WXEO Wun
 */





// POST FORMAT METABOX

// function wxeo_add_admin_metabox_styles() {
//   wp_enqueue_style( 'wxeo-admin-metabox',  get_template_directory().'admin/css/admin.css' );
// }
// add_action( 'admin_enqueue_scripts', 'wxeo_add_admin_metabox_styles' );


$metaboxes = array(

    'link'    => array(
        'title'             => __('Link Post', 'twentyeleven'),
        'applicableto'      => 'post',
        'location'          => 'normal',
        'display_condition' => 'post-format-link',
        'priority'          => 'high',
        'fields'            => array(
            'l_url' => array(
                'title'       => __('link url:', 'twentyeleven'),
                'default'     => __('Nothing here.', 'twentyeleven'),
                'type'        => 'text',
                'description' => '',
            ),
        )
    ),

    'quote'   => array(
        'title'             => __('Quote Post', 'twentyeleven'),
        'applicableto'      => 'post',
        'location'          => 'normal',
        'display_condition' => 'post-format-quote',
        'priority'          => 'high',
        'fields'            => array(

          'quote_author'  => array(
              'title'       => __('Quote Author:', 'twentyeleven'),
              'default'     => __('Richard Branson', 'twentyeleven'),
              'type'        => 'text',
              'description' => 'Who authored the quote?',
          ),
          'quote'         => array(
              'title'       => __('quote author:', 'twentyeleven'),
              'default'     => __('Screw It, Let\'s Do It', 'twentyeleven'),
              'type'        => 'textarea',
              'description' => '',
          ),
        )
    ),

    'audio'   => array(
        'title'             => __('Audio Post', 'twentyeleven'),
        'applicableto'      => 'post',
        'location'          => 'normal',
        'display_condition' => 'post-format-audio',
        'priority'          => 'high',
        'fields'            => array(

          'mp3_audio_link'    => array(
            'title'       => __('MP3 URL', 'twentyeleven'),
            'default'     => __('http://www.w3schools.com/html/horse.mp3', 'twentyeleven'),
            'type'        => 'media',
            'file_type'   => 'audio',
            'btn_txt'     => __('Choose or Upload an MP3 File', 'twentyeleven'),
            'description' => '',
          ),
          'ogg_audio_link'    => array(
            'title'       => __('OGG URL', 'twentyeleven'),
            'default'     => __('http://www.w3schools.com/html/horse.ogg', 'twentyeleven'),
            'type'        => 'media',
            'file_type'   => 'audio',
            'btn_txt'     => __('Choose or Upload an OGG File', 'twentyeleven'),
            'description' => '',
          ),
        )
    ),


    'video'   => array(
        'title'             => __('Video Post', 'twentyeleven'),
        'applicableto'      => 'post',
        'location'          => 'normal',
        'display_condition' => 'post-format-video',
        'priority'          => 'high',
        'fields'            => array(

          'mp4_video_link'    => array(
            'title'       => __('MP4 URL', 'twentyeleven'),
            'default'     => __('http://www.w3schools.com/html/movie.mp4', 'twentyeleven'),
            'type'        => 'media',
            'file_type'   => 'audio',
            'btn_txt'     => __('Choose or Upload an MP4 File', 'twentyeleven'),
            'description' => '',
          ),
          'ogg_video_link'    => array(
            'title'       => __('OGG URL', 'twentyeleven'),
            'default'     => __('http://www.w3schools.com/html/movie.ogg', 'twentyeleven'),
            'type'        => 'media',
            'file_type'   => 'audio',
            'btn_txt'     => __('Choose or Upload an OGG File', 'twentyeleven'),
            'description' => '',
          ),
          'video_preview_image'    => array(
            'title'       => __('Video Preview Image', 'twentyeleven'),
            'default'     => __('http://peach.blender.org/wp-content/uploads/bbb-splash.png', 'twentyeleven'),
            'type'        => 'media',
            'file_type'   => 'image',
            'btn_txt'     => __('Choose or Upload an Image File', 'twentyeleven'),
            'description' => '',
          ),
          'embed_code'         => array(
              'title'       => __('External Video Site Embed', 'twentyeleven'),
              'default'     => __('<iframe width="420" height="315" src="//www.youtube.com/embed/_OBlgSz8sSM" frameborder="0" allowfullscreen></iframe>', 'twentyeleven'),
              'type'        => 'textarea',
              'description' => '',
          ),

        )
    ),





);




/**
 * Adds a meta box to the post editing screen
 */
function add_post_format_metabox() {
    global $metaboxes;
 
    if ( ! empty( $metaboxes ) ) {
        foreach ( $metaboxes as $id => $metabox ) {
            add_meta_box( $id, $metabox['title'], 'show_metaboxes', $metabox['applicableto'], $metabox['location'], $metabox['priority'], $id );
        }
    }
}
add_action( 'admin_init', 'add_post_format_metabox' );



function show_metaboxes( $post, $args ) {
    global $metaboxes;
  
    wp_enqueue_style( 'wxeo-admin-metabox',  get_template_directory_uri() .'/admin/css/admin.css' );

    wp_nonce_field( basename( __FILE__ ), 'prfx_nonce' );
    $stored_meta = get_post_meta( $post->ID );

    // $custom = get_post_custom( $post->ID );
    $fields = $tabs = $metaboxes[$args['id']]['fields'];
    

    /** Nonce **/
    $output = '<div class="wxeo-admin-metabox-form-box">';
    $output .= '<input type="hidden" name="post_format_meta_box_nonce" value="' . wp_create_nonce( basename( __FILE__ ) ) . '" />';

    if ( sizeof( $fields ) ) {
      $prfx_stored_meta = '';
      foreach ( $fields as $id => $field ) {

          if ( isset ( $stored_meta[$id][0] ) ) { $prfx_stored_meta = $stored_meta[$id][0]; }

          $output .= '
            <div class="wxeo-admin-metabox-form">
              <div class="wxeo-admin-metabox-info">
                <label>' . $field['title'] . '</label>
                <p>' . $field['description'] . '</p>
              </div>
              <div class="wxeo-admin-metabox-field">
          ';

          switch ( $field['type'] ) {
              // default:
              case "text":
                $output .= '<input class="' . $id . ' ' . $field['type'] . '" type="text" name="' . $id . '" placeholder="'. $field['default'] .'" value="' . $prfx_stored_meta . '" />';
                break;

              case "textarea":                 
                $output .= '<textarea rows="5" class="' . $id . ' ' . $field['type'] . '" type="text" name="' . $id . '" placeholder="'. $field['default'] .'" >' . $prfx_stored_meta . '</textarea>';
                break;

              case 'color':
                $output .= '<input class="meta-color ' . $id . ' ' . $field['type'] . '" name="' . $id . '" type="text" value="' . $prfx_stored_meta . '" />';
                break;

              case 'media':
                $output .= '<input type="text" name="' . $id . '" class="' . $field['type'] . '" value="' . $prfx_stored_meta . '" id="meta-media-' . $id . '" />';
                $output .= '<button type="button" data-field-id="' . $id . '" data-file-type="' . $field['file_type'] . '" class="button meta-media-button ' . $field['type'] . '">' . $field['btn_txt'] . '</button>';
                break;
          }

          $output .= '
              </div>
            </div>
          ';


      }
    }
    $output .= '</div>';
    echo $output;
}


add_action( 'save_post', 'save_metaboxes' );
 
function save_metaboxes( $post_id ) {
    global $metaboxes;
    
    // Checks save status
    $is_autosave = wp_is_post_autosave( $post_id );
    $is_revision = wp_is_post_revision( $post_id );
    $is_valid_nonce = ( isset( $_POST[ 'post_format_meta_box_nonce' ] ) && wp_verify_nonce( $_POST[ 'post_format_meta_box_nonce' ], basename( __FILE__ ) ) ) ? 'true' : 'false';
   
    // Exits script depending on save status
    if ( $is_autosave || $is_revision || !$is_valid_nonce ) {
      return;
    }

    $post_type = get_post_type();

    // loop through fields and save the data
    foreach ( $metaboxes as $id => $metabox ) {
        // check if metabox is applicable for current post type
        if ( $metabox['applicableto'] == $post_type ) {
            $fields = $metaboxes[$id]['fields'];
              
            foreach ( $fields as $id => $field ) {
                $old = get_post_meta( $post_id, $id, true );
                $new = $_POST[$id];

                if ( $new && $new != $old ) {
                    update_post_meta( $post_id, $id, $new );
                }
                elseif ( '' == $new && $old || ! isset( $_POST[$id] ) ) {
                    delete_post_meta( $post_id, $id, $old );
                }
            }
        }
    }
}

add_action( 'admin_print_scripts', 'display_metaboxes', 1000 );


function display_metaboxes() {
    global $metaboxes;
    if ( get_post_type() == "post" ) :
        ?>
        <script type="text/javascript">// <![CDATA[
            $ = jQuery;
 
            <?php
            $formats = $ids = array();
            foreach ( $metaboxes as $id => $metabox ) {
                array_push( $formats, "'" . $metabox['display_condition'] . "': '" . $id . "'" );
                array_push( $ids, "#" . $id );
            }
            ?>

            var formats = { <?php echo implode( ',', $formats );?> };
            var ids = "<?php echo implode( ',', $ids ); ?>";


            function displayMetaboxes() {
                // Hide all post format metaboxes
                $(ids).hide();
                // Get current post format
                var selectedElt = $("input[name='post_format']:checked").attr("id");
                
                // If exists, fade in current post format metabox
                if ( formats[selectedElt] )
                  $("." + formats[selectedElt]).fadeIn();
                  $("#" + formats[selectedElt]).fadeIn();
            }
 
            $(function() {
                // Show/hide metaboxes on page load
                displayMetaboxes();
 
                // Show/hide metaboxes on change event
                $("input[name='post_format']").change(function() {
                    displayMetaboxes();
                });
            });
 
        // ]]></script>
        <?php
    endif;
}






/**
 * Adds the meta box stylesheet when appropriate
 */
function prfx_admin_styles(){
  global $typenow;
  if( $typenow == 'post' ) {
    wp_enqueue_style( 'prfx_meta_box_styles', get_template_directory_uri() . '/admin/css/meta-box-styles.css' );
  }
}
add_action( 'admin_print_styles', 'prfx_admin_styles' );


/**
 * Loads the color picker javascript
 */
function prfx_color_enqueue() {
  global $typenow;
  if( $typenow == 'post' ) {
    wp_enqueue_style( 'wp-color-picker' );
    wp_enqueue_script( 'meta-box-color-js', get_template_directory_uri() . '/admin/js/meta-box-color.js', array( 'wp-color-picker' ) );
  }
}
add_action( 'admin_enqueue_scripts', 'prfx_color_enqueue' );

/**
 * Loads the image management javascript
 */
function prfx_image_enqueue() {
  global $typenow;
  if( $typenow == 'post' ) {
    wp_enqueue_media();
 
    // Registers and enqueues the required javascript.
    wp_register_script( 'meta-box-image', get_template_directory_uri() . '/admin/js/meta-box-image.js', array( 'jquery' ) );
    wp_localize_script( 'meta-box-image', 'meta_image',
      array(
        'title' => __( 'Choose or Upload an Image', 'prfx-textdomain' ),
        'button' => __( 'Use this image', 'prfx-textdomain' ),
      )
    );
    wp_enqueue_script( 'meta-box-image' );
  }
}
add_action( 'admin_enqueue_scripts', 'prfx_image_enqueue' );



// 2. Each post format has its own set of validations and optional tags.







// add_action( 'wp_enqueue_scripts', 'my_enqueue_styles' );

// function my_enqueue_styles() {
//     wp_enqueue_style( 
//         'my-mediaelement', 
//         trailingslashit( get_template_directory_uri() ) . 'admin/css/style_media.css',
//         null,
//         '20130902'
//     );
// }

// add_action( 'wp_enqueue_scripts', 'my_deregister_styles' );

// function my_deregister_styles() {
//     wp_deregister_style( 'mediaelement' );
//     wp_deregister_style( 'wp-mediaelement' );
// }