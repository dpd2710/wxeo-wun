<?php
/**
 * Portfolio
 ******************************************/


add_action('init', 'project_custom_init');  

function project_custom_init() {

    $labels = array(
        'name' => _x('portfolio', 'post type general name'),
        'singular_name' => _x('portfolio-image', 'post type singular name'),
        'add_new' => _x('Add New', 'portfolio-image'),
        'add_new_item' => __('Add New Portfolio'),
        'edit_item' => __('Edit Portfolio'),
        'new_item' => __('New Portfolio'),
        'view_item' => __('View Portfolio'),
        'search_items' => __('Search Portfolio'),
        'not_found' =>  __('No Portfolio Found'),
        'not_found_in_trash' => __('No Portfolio Found in Trash'),
        'parent_item_colon' => '',
        'menu_name' => 'Portfolio',
    );

    $args = array(
        'labels' => $labels,
        'public' => true,
        'publicly_queryable' => true,
        'show_ui' => true,
        'show_in_menu' => true,
        'query_var' => true,
        'rewrite' => true,
        'capability_type' => 'post',
        'has_archive' => true,
        'hierarchical' => false,
        'menu_position' => null,
        'singular_name' => 'portfolio-image',
        'supports' => array('title','editor','author','thumbnail','comments')
    );
    // The following is the main step where we register the post.
    register_post_type('portfolio-image',$args);

    // Initialize New Taxonomy Labels
    $labels = array(
        'name' => _x( 'Tags', 'taxonomy general name' ),
        'singular_name' => _x( 'Tag', 'taxonomy singular name' ),
        'search_items' =>  __( 'Search Types' ),
        'all_items' => __( 'All Tags' ),
        'parent_item' => __( 'Parent Tag' ),
        'parent_item_colon' => __( 'Parent Tag:' ),
        'edit_item' => __( 'Edit Tags' ),
        'update_item' => __( 'Update Tag' ),
        'add_new_item' => __( 'Add New Tag' ),
        'new_item_name' => __( 'New Tag Name' ),
    );
    // Custom taxonomy for Project Tags
    register_taxonomy('tagportfolio',array('portfolio-image'), array(
        'hierarchical' => true,
        'labels' => $labels,
        'show_ui' => true,
        'query_var' => true,
        'rewrite' => array( 'slug' => 'tag-portfolio' ),
    ));

} 

add_shortcode( 'portfolio', 'rmcc_post_listing_shortcode1' );
function rmcc_post_listing_shortcode1( $atts ) {

    $attributes = shortcode_atts( array(
        'title' => 'Title Goes here',
        'popup' => true,
    ), $atts );
	
	
    ob_start();
    $taxonomies = array('tagportfolio');

    $args = array(
        'orderby'       => 'name', 
        'order'         => 'ASC',
        'hide_empty'    => true, 
        'exclude'       => array(), 
        'exclude_tree'  => array(), 
        'include'       => array(),
        'number'        => '', 
        'fields'        => 'all', 
        'slug'          => '', 
        'parent'         => '',
        'hierarchical'  => true, 
        'child_of'      => 0, 
        'get'           => '', 
        'name__like'    => '',
        'pad_counts'    => false, 
        'offset'        => '', 
        'search'        => '', 
        'cache_domain'  => 'core'
    ); 


    $terms = get_terms( $taxonomies, $args );
    
    // if ( !empty( $terms ) && !is_wp_error( $terms ) ){
        echo '<ul id="filter"> <li class="current"><a href="#">All</a></li>';
        foreach ( $terms as $term ) {
            echo '<li><a href="#">' . $term->name . '</a></li>';
        }
        if($attributes['popup']=='true'){$class= "popup-gallery";}else{$class= "";}
        echo '</ul><ul id="portfolio" class="'.$class.'">';
    // }
 	$query = new WP_Query( array(
        'post_type' => 'portfolio-image',
        'posts_per_page' => -1,
        'order' => 'ASC',
        'orderby' => 'title',
    ) );
    if ( $query->have_posts() ) {

      while ( $query->have_posts() ) : $query->the_post(); 
			$term_list = wp_get_post_terms(get_the_ID(), 'tagportfolio', array("fields" => "names"));
			$str = $url = ''; 
      // $post_thumbnail_id = get_post_thumbnail_id(get_the_ID());

      foreach($term_list as $sterm) {
        $str .= " ".strtolower(str_replace(' ', '-', $sterm));
			}			
			
			if($attributes['popup']=='true') {
  			$url  = wp_get_attachment_image_src( get_post_thumbnail_id(get_the_ID()), 'full' );
  			$url = $url['0'];
			}else{
			 $url = get_attachment_link(get_the_ID());
			}
			
			
			?>
      
      <li class="<?php echo $str; ?>"><a href="<?php echo $url;  ?>" >
        <?php  the_post_thumbnail('thumbnail'); ?></a>
      </li>
			<?php endwhile; }?>
			

       
        
    </ul>
    <link rel="stylesheet" href="http://dimsemenov-static.s3.amazonaws.com/dist/magnific-popup.css">
    <script src="//ajax.googleapis.com/ajax/libs/jquery/1.11.1/jquery.min.js"></script>
    <script src="http://dimsemenov-static.s3.amazonaws.com/dist/jquery.magnific-popup.min.js"></script> 
    <script>
    $(document).ready(function() {
    $('.popup-gallery').magnificPopup({
    		delegate: 'a',
    		type: 'image',
    		tLoading: 'Loading image #%curr%...',
    		mainClass: 'mfp-img-mobile',
    		gallery: {
    			enabled: true,
    			navigateByImgClick: true,
    			preload: [0,1] // Will preload 0 - before current, and 1 after the current image
    		},
    		image: {
    			tError: '<a href="%url%">The image #%curr%</a> could not be loaded.',
    			titleSrc: function(item) {
    				//return item.el.attr('title') + '<small>by Marsel Van Oosten</small>';
    			}
    		}
    	});
    	
    	
        $('ul#filter a').click(function() {
            $(this).css('outline','none');
            $('ul#filter .current').removeClass('current');
            $(this).parent().addClass('current');         
            var filterVal = $(this).text().toLowerCase().split(' ').join('-');                
            if(filterVal == 'all') {
                $('ul#portfolio li.hidden').fadeIn('slow').removeClass('hidden');
            } else {
    		         $('ul#portfolio li').each(function() {
                    if(!$(this).hasClass(filterVal)) {
                        $(this).fadeOut('normal').addClass('hidden');
                    } else {
                        $(this).fadeIn('slow').removeClass('hidden');
                    }
                });
            }
             
            return false;
        });
    });
    </script>
    <style>
    ul#filter {
        float: left;
        font-size: 16px;
        list-style: none;
        margin-left: 0;
        width: 100%;
    }
    ul#filter li {
        border-right: 1px solid #dedede;
        float: left;
        line-height: 16px;
        margin-right: 10px;
        padding-right: 10px;
    }
    ul#filter li:last-child { border-right: none; margin-right: 0; padding-right: 0; }
    ul#filter a { color: #999; text-decoration: none; }
    ul#filter li.current a, ul#filter a:hover { text-decoration: underline; }
    ul#filter li.current a { color: #333; font-weight: bold; }
    ul#portfolio {
        float: left;
        list-style: none;
        margin-left: 0;
        width: 672px;
    }
    ul#portfolio li {
        border: 1px solid #dedede;
        float: left;
        margin: 0 10px 10px 0;
        padding: 5px;
        width: 202px;
    }
    ul#portfolio a { display: block; width: 100%; }
    ul#portfolio a:hover { text-decoration: none; }
    ul#portfolio img { border: 1px solid #dedede; display: block; padding-bottom: 5px; }
    </style>


    <?php
    wp_reset_postdata();
    $myvariable = ob_get_clean();
    return $myvariable.$attributes['title'];
    
}


