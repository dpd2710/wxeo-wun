<?php

add_shortcode( 'feature-list-centered', 'feature_list_centered' );
function feature_list_centered( $atts ) {
  extract( shortcode_atts( array(
    'size' => 'sm',
    'icon' => 'fa fa-heart',
    'title' => 'Retina ready',
    'content' => 'Omega is designed with the latest technologies so that your website will look sharp and stunning even on high resolution retina displays.',
  ), $atts ) );

  $output_html = '

  <div class="feature-centered-list '.$size.'">
    <div class="icon-box">
      <i class="'.$icon.'"></i>
    </div>
    <div class="feature-list-content">
      <h3>'.$title.'</h3>
      <p>'.$content.'</p>
    </div>
  </div>

  ';

  return $output_html;
}



add_action( 'init', 'VC_feature_list_centered' );
function VC_feature_list_centered() {
   vc_map( array(
      "name" => __("Feature List Centered"),
      "base" => "feature-list-centered",
      "class" => "",
      "category" => __('Content'),
      // 'admin_enqueue_js' => array(get_template_directory_uri().'/vc_extend/bartag.js'),
      // 'admin_enqueue_css' => array(get_template_directory_uri().'/vc_extend/bartag.css'),
      "params" => array(
         array(
            "type" => "dropdown",
            "holder" => "div",
            "class" => "",
            "heading" => __("Size"),
            "param_name" => "size",
            "value" => array('Small'=>'sm', 'Medium'=>'md', 'Large'=>'lg'),
            "description" => __("Description for foo param.")
         ),

         array(
            "type" => "dropdown",
            "holder" => "div",
            "class" => "",
            "heading" => __("Icon"),
            "param_name" => "icon",
            "value" => array('Small'=>'fa fa-times', 'Medium'=>'fa fa-search', 'Large'=>'fa fa-heart'),
            "description" => __("Description for foo param.")
         ),

         array(
            "type" => "textfield",
            "holder" => "div",
            "class" => "",
            "heading" => __("Title"),
            "param_name" => "title",
            "value" => __("Default params value"),
            "description" => __("Description for foo param.")
         ),


         array(
            "type" => "textarea",
            "holder" => "div",
            "class" => "",
            "heading" => __("Content"),
            "param_name" => "content",
            "value" => __("Default params value"),
            "description" => __("Description for foo param.")
         ),

      )
   ) );
}







