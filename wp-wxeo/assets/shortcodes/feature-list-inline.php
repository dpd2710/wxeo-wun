<?php

add_shortcode( 'feature-list-inline', 'feature_list_inline' );
function feature_list_inline( $atts ) {
  extract( shortcode_atts( array(
    'size' => 'sm',
    'position'=> 'lt',
    'icon' => 'fa fa-heart',
    'title' => 'Retina ready',
    'content' => 'Omega is designed with the latest technologies so that your website will look sharp and stunning even on high resolution retina displays.',
  ), $atts ) );

  $output_html = '

  <div class="feature-inline-list '.$position.' '.$size.'">
    <div class="feature-list-content">
      <h3><div class="icon-box"><i class="'.$icon.'"></i></div>'.$title.'</h3>
      <p>'.$content.'</p>
    </div>
  </div>

  ';

  return $output_html;
}




add_action( 'init', 'VC_feature_list_inline' );
function VC_feature_list_inline() {
   vc_map( array(
      "name" => __("Feature List Inline"),
      "base" => "feature-list-inline",
      "class" => "",
      "category" => __('Content'),
      "params" => array(
         array(
            "type" => "dropdown",
            "holder" => "div",
            "class" => "",
            "heading" => __("Size"),
            "param_name" => "size",
            "value" => array('Small'=>'sm', 'Medium'=>'md', 'Large'=>'lg'),
            "description" => __("Description for foo param.")
         ),

         array(
            "type" => "dropdown",
            "holder" => "div",
            "class" => "",
            "heading" => __("Position"),
            "param_name" => "position",
            "value" => array('Left'=>'lt', 'Right'=>'rt'),
            "description" => __("Description for foo param.")
         ),
         
         array(
            "type" => "dropdown",
            "holder" => "div",
            "class" => "",
            "heading" => __("Icon"),
            "param_name" => "icon",
            "value" => array('Small'=>'fa fa-times', 'Medium'=>'fa fa-search', 'Large'=>'fa fa-heart'),
            "description" => __("Description for foo param.")
         ),

         array(
            "type" => "textfield",
            "holder" => "div",
            "class" => "",
            "heading" => __("Title"),
            "param_name" => "title",
            "value" => __("Default params value"),
            "description" => __("Description for foo param.")
         ),


         array(
            "type" => "textarea",
            "holder" => "div",
            "class" => "",
            "heading" => __("Content"),
            "param_name" => "content",
            "value" => __("Default params value"),
            "description" => __("Description for foo param.")
         ),

      ),


   ) );
}


