<?php

add_shortcode( 'milestone-counter', 'milestone_counter' );
function milestone_counter( $atts ) {
  wp_enqueue_script('waypoints', get_template_directory_uri() . '/js/waypoints.min.js', array('jquery'), false, true);
  wp_enqueue_script('counterup', get_template_directory_uri() . '/js/counterup.min.js', array('jquery', 'waypoints'), false, true);
  
  extract( shortcode_atts( array(
    'icon'  => 'fa fa-heart',
    'icon_size'  => 'sm',
    'title' => 'Retina ready',
    'pre_count'    => '$',
    'post_count'    => '/year',
    'to'    => 150,
  ), $atts ) );

  $output_html = '
  <div class="count_up_counter lt rt">
    <div class="icon-box '.$icon_size.'">
      <i class="'.$icon.'"></i>
    </div>
    <div class="counter-content">
      <span>'.$pre_count.'</span><span class="counter">'.$to.'</span><span>'.$post_count.'</span>
      <h4>'.$title.'</h4>
    </div>
  </div>
  ';

  return $output_html;
}



add_action( 'init', 'VC_milestone_counter' );
function VC_milestone_counter() {
   vc_map( array(
      "name" => __("Milestone Counter"),
      "base" => "milestone-counter",
      "category" => __('Content'),
      "params" => array(
         array(
            "type" => "dropdown",
            "holder" => "div",
            "class" => "",
            "heading" => __("Icon Size"),
            "param_name" => "icon_size",
            "value" => array('Extra Small'=>'xs', 'Small'=>'sm', 'Medium'=>'md', 'Large'=>'lg'),
            "description" => __("Description for foo param.")
         ),

         array(
            "holder" => "div",
            "type" => "dropdown",
            "class" => "",
            "heading" => __("Icon"),
            "param_name" => "icon",
            "value" => array('Small'=>'fa fa-times', 'Medium'=>'fa fa-wordpress', 'Large'=>'fa fa-heart'),
            "description" => __("Description for foo param.")
         ),

         array(
            "type" => "textfield",
            "holder" => "div",
            "class" => "",
            "heading" => __("Title"),
            "param_name" => "title",
            "value" => __("Default params value"),
            "description" => __("Description for foo param.")
         ),


         array(
            "type" => "textfield",
            "holder" => "div",
            "class" => "",
            "heading" => __("Pre Count"),
            "param_name" => "pre_count",
            "description" => __("Description for foo param.")
         ),

         array(
            "type" => "textfield",
            "holder" => "div",
            "class" => "",
            "heading" => __("Count Value"),
            "param_name" => "to",
            "value" => 100,
            "description" => __("Description for foo param.")
         ),

         array(
            "type" => "textfield",
            "holder" => "div",
            "class" => "",
            "heading" => __("Post Count"),
            "param_name" => "post_count",
            "description" => __("Description for foo param.")
         ),



      )
   ) );
}



