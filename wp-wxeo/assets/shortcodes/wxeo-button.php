<?php
add_shortcode( 'wxeo-button', 'wxeo_button' );
function wxeo_button( $atts ) {	 
extract( shortcode_atts( array(
    'text' => 'Button',
    'class'=> 'btn-xl btn-box',    
  ), $atts ) );
  $output_html = '<a class="btn '.$class.'">'.$text.'</a>';
  return $output_html;
}
?>