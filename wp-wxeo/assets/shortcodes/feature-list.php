<?php

add_shortcode( 'feature-list', 'feature_list' );
function feature_list( $atts ) {
  extract( shortcode_atts( array(
    'size' => 'sm',
    'position'=> 'lt',
    'icon' => 'fa fa-heart',
    'title' => 'Retina ready',
    'content' => 'Omega is designed with the latest technologies so that your website will look sharp and stunning even on high resolution retina displays.',
  ), $atts ) );

  $output_html = '

  <div class="feature-list '.$position.' '.$size.'">
    <div class="icon-box">
      <i class="'.$icon.'"></i>
    </div>
    <div class="feature-list-content">
      <h3>'.$title.'</h3>
      <p>'.$content.'</p>
    </div>
  </div>

  ';

  return $output_html;
}



add_action( 'init', 'VC_feature_list' );
function VC_feature_list() {
   vc_map( array(
      "name" => __("Feature List"),
      "base" => "feature-list",
      "class" => "feature-list",
      "category" => __('Content'),
      'admin_enqueue_js' => array(get_template_directory_uri().'/admin/js/select2.min.js'),
      'admin_enqueue_css' => array(get_template_directory_uri().'/admin/css/select2.css'),
      "params" => array(
         array(
            "type" => "dropdown",
            "holder" => "div",
            "class" => "",
            "heading" => __("Size"),
            "param_name" => "size",
            "value" => array('Small'=>'sm', 'Medium'=>'md', 'Large'=>'lg'),
            "description" => __("Description for foo param.")
         ),

         array(
            "type" => "dropdown",
            "holder" => "div",
            "class" => "",
            "heading" => __("Position"),
            "param_name" => "position",
            "value" => array('Left'=>'lt', 'Right'=>'rt'),
            "description" => __("Description for foo param.")
         ),

         array(
            "type" => "dropdown",
            "holder" => "div",
            "class" => "",
            "heading" => __("Icon"),
            "param_name" => "icon",
            "value" => array('Yahoo'=>'fa fa-yahoo', 'Medium'=>'fa fa-search', 'Large'=>'fa fa-heart'),
            "description" => __("Description for foo param.")
         ),

         array(
            "type" => "textfield",
            "holder" => "div",
            "class" => "",
            "heading" => __("Title"),
            "param_name" => "title",
            "value" => __("Default params value"),
            "description" => __("Description for foo param.")
         ),


         array(
            "type" => "textarea",
            "holder" => "div",
            "class" => "",
            "heading" => __("Content"),
            "param_name" => "content",
            "value" => __("Default params value"),
            "description" => __("Description for foo param.")
         ),

      )
   ) );
}







