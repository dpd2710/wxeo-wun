<?php

	global $post;
	$meta = get_post_meta($post->ID);
	$output = '';



	if( isset($meta['quote_author'][0]) || isset($meta['quote'][0]) ) {

		if ( isset($meta['quote'][0]) ) {
			$output .= '<h3>'.$meta['quote'][0].'</h3>';
		}
		if ( isset($meta['quote_author'][0]) ) {
			$output .= '<small>'.$meta['quote_author'][0].'</small>';
		}

	}

	echo $output;

?>