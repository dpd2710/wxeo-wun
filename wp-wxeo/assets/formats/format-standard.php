<?php
if (is_single()) {
	$post_image = blog_image_size('single');
} else {
	$post_image = blog_image_size();
}
echo the_post_thumbnail( $post_image,  array('class'	=> "full-img") );
?>
