<?php

	global $post;
	$meta = get_post_meta($post->ID);
	$output = '';


	if( isset($meta['mp3_audio_link'][0]) || isset($meta['ogg_audio_link'][0]) ) {

		$output .= '<audio style="width:100%;" controls>';
		if ( isset($meta['mp3_audio_link'][0]) ) {
			$output .= '<source src="'.$meta['mp3_audio_link'][0].'" type="audio/mpeg">';
		}
		if ( isset($meta['ogg_audio_link'][0]) ) {
			$output .= '<source src="'.$meta['ogg_audio_link'][0].'" type="audio/ogg">';
		}
		$output .= '</audio>';

	}

	echo $output;




?>

