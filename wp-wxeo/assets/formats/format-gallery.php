<?php 
if (is_single()) {
	$post_image = blog_image_size('single');
} else {
	$post_image = blog_image_size();
}
?>
<ul class="rslides">
	  	<?php

			global $post; // may not be necessary unless you have scope issues
			              // for example, this is inside a function
			$post_content = $post->post_content;
			preg_match('/\[gallery.*ids=.(.*).\]/', $post_content, $ids);
			$array_id = explode(",", $ids[1]);
			foreach($array_id as $singleID) {
				$thumb_img = get_post( $singleID );
        echo '<li>';
				echo wp_get_attachment_image($singleID, $post_image);
        
        if ($thumb_img->post_excerpt !== '') {
  				echo '<p class="caption">';
          echo $thumb_img->post_excerpt;
          echo '</p>';
        }
        echo '</li>';
			}

			?>

</ul>


