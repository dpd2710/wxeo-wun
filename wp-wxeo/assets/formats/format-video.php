<?php

	global $post;
	$meta = get_post_meta($post->ID);
	$output = '';

	if( get_post_format() === 'video' && isset($meta['mp4_video_link'][0]) || isset($meta['ogg_video_link'][0]) ) {

		$output .= '<video style="width:100%;" controls>';
		if ( isset($meta['mp4_video_link'][0]) ) {
			$output .= '<source src="'.$meta['mp4_video_link'][0].'" type="video/mp4">';
		}
		if ( isset($meta['ogg_video_link'][0]) ) {
			$output .= '<source src="'.$meta['ogg_video_link'][0].'" type="video/ogg">';
		}
		$output .= '</video>';

	}

	echo $output;
?>