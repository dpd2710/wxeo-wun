<?php
/**
 * @package WXEO Wun
 */

wp_link_pages( array(
	'before' => '<div class="page-links">' . __( 'Pages:', 'wxeo' ),
	'after'  => '</div>',
) );
?>

<?php 

	switch (wxeo_wun('wxeo-blog-single-timestamp')) {
		case  'top-left':
			$timestamp = 'blog-timestamp-block-left';
			break;
		default:
			$timestamp = 'blog-timestamp-content-left';
			break;
	}


?>
<article id="post-<?php the_ID(); ?>" <?php post_class( array('blog', 'blog-classic', 'blog-timestamp', $timestamp, wxeo_wun('wxeo-blog-single-timestamp-style'), 'author-pic-article') ); ?>>
	<?php if (wxeo_wun('wxeo-blog-single-timestamp') === 'top-left'): ?>
	<div class="pull-left">
		<div class="timestamp"><time datetime="2014-06-24T22:45:25+00:00" class="updated">20<span>Feb</span><!-- <span class="year">2014</span> --></time></div>
	</div>
	<div class="blog-content">
	<?php endif ?>
	<div class="article-img">
		
		<?php 
			if (get_post_type() == 'portfolio-image') {
				echo get_the_post_thumbnail(get_the_ID(), 'blog-image-medium-crop', array('class'	=> "full-img"));
			} else {
				if(!get_post_format()) {
					get_template_part('wp-wxeo/assets/formats/format', 'standard');
				} else {
					get_template_part('wp-wxeo/assets/formats/format', get_post_format());
				}
			}
		?>
		<?php if (wxeo_wun('wxeo-blog-single-timestamp') === 'over-image'): ?>
			<div class="timestamp"><time datetime="2014-06-24T22:45:25+00:00" class="updated">20<span>Feb</span><!-- <span class="year">2014</span> --></time></div>
		<?php endif ?>
		<div class="author-pic-center"><?php echo get_avatar( get_the_author_meta( 'ID' ), 70 ); ?></div>
	</div>
	
	<?php if (wxeo_wun('wxeo-blog-single-timestamp') === 'content-left'): ?>
	<div class="timestamp pull-left"><time datetime="2014-06-24T22:45:25+00:00" class="updated">20<span>Feb</span><!-- <span class="year">2014</span> --></time></div>
	<?php endif ?>

	<?php if (wxeo_wun('wxeo-blog-single-timestamp') != 'top-left'): ?>
  <div class="blog-content">
  <?php endif ?>
    <?php the_title( '<h2 class="entry-title">', '</h2>' ); ?>
    <p><?php the_content(); ?></p>
    <div class="entry-meta">
    	<a class="btn btn-border btn-sm read-more">Read More</a>
			<span class="byline"> by <span class="author vcard"><a class="url fn n" href="#">admin</a></span></span>
			<span class="post-meta-devide"> | </span>
			<?php if (wxeo_wun('wxeo-blog-single-timestamp') === 'meta'): ?>
				<span class="meta meta-date"><i class="fa fa-pencil"></i><time datetime="2014-06-24T22:45:25+00:00" class="updated">February 20, 2014</time></span>
				<span class="post-meta-devide"> | </span>
			<?php endif ?>
			<a href="#" title="View all posts in Uncategorized" rel="category tag">Uncategorized</a>
			<span class="post-meta-devide"> | </span>
			Tag: <a href="#" rel="tag">sticky</a>, <a href="#" rel="tag">template</a>
		</div>
	</div>


<!-- 	<footer class="entry-footer">
		<?php
			/* translators: used between list items, there is a space after the comma */
			$category_list = get_the_category_list( __( ', ', 'wxeo' ) );

			/* translators: used between list items, there is a space after the comma */
			$tag_list = get_the_tag_list( '', __( ', ', 'wxeo' ) );

			if ( ! wxeo_categorized_blog() ) {
				// This blog only has 1 category so we just need to worry about tags in the meta text
				if ( '' != $tag_list ) {
					$meta_text = __( 'This entry was tagged %2$s. Bookmark the <a href="%3$s" rel="bookmark">permalink</a>.', 'wxeo' );
				} else {
					$meta_text = __( 'Bookmark the <a href="%3$s" rel="bookmark">permalink</a>.', 'wxeo' );
				}

			} else {
				// But this blog has loads of categories so we should probably display them here
				if ( '' != $tag_list ) {
					$meta_text = __( 'This entry was posted in %1$s and tagged %2$s. Bookmark the <a href="%3$s" rel="bookmark">permalink</a>.', 'wxeo' );
				} else {
					$meta_text = __( 'This entry was posted in %1$s. Bookmark the <a href="%3$s" rel="bookmark">permalink</a>.', 'wxeo' );
				}

			} // end check for categories on this blog

			printf(
				$meta_text,
				$category_list,
				$tag_list,
				get_permalink()
			);
		?>

		<?php edit_post_link( __( 'Edit', 'wxeo' ), '<span class="edit-link">', '</span>' ); ?>
	</footer> -->
	
</article>
