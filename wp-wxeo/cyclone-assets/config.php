<?php

/**
  ReduxFramework Sample Config File
  For full documentation, please visit: https://docs.reduxframework.com
 * */

if (!class_exists('WXEO_Wun_Options')) {

    class WXEO_Wun_Options {

        public $args        = array();
        public $sections    = array();
        public $theme;
        public $ReduxFramework;

        public function __construct() {

            // echo "<pre>";
            // var_dump(debug_backtrace());
            // echo "</pre>";

            if (!class_exists('ReduxFramework')) {
                return;
            }

            // This is needed. Bah WordPress bugs.  ;)
            if (  true == Redux_Helpers::isTheme(__FILE__) ) {
                $this->initSettings();
            } else {
                add_action('plugins_loaded', array($this, 'initSettings'), 10);
            }

        }

        public function initSettings() {

            // Set the default arguments
            $this->setArguments();

            // Create the sections and fields
            $this->setSections();

            if (!isset($this->args['opt_name'])) { // No errors please
                return;
            }

            // If Redux is running as a plugin, this will remove the demo notice and links
            //add_action( 'redux/loaded', array( $this, 'remove_demo' ) );
            
            // Function to test the compiler hook and demo CSS output.
            // Above 10 is a priority, but 2 in necessary to include the dynamically generated CSS to be sent to the function.
            add_filter('redux/options/'.$this->args['opt_name'].'/compiler', array( $this, 'compiler_action' ), 10, 3);

            add_filter('redux/options/'.$this->args['opt_name'].'/compilerjs', array( $this, 'compiler_action_js' ), 10, 3);
            
            // Change the arguments after they've been declared, but before the panel is created
            //add_filter('redux/options/'.$this->args['opt_name'].'/args', array( $this, 'change_arguments' ) );
            
            // Change the default value of a field after it's been set, but before it's been useds
            //add_filter('redux/options/'.$this->args['opt_name'].'/defaults', array( $this,'change_defaults' ) );
            
            // Dynamically add a section. Can be also used to modify sections/fields
            //add_filter('redux/options/' . $this->args['opt_name'] . '/sections', array($this, 'dynamic_section'));

            $this->ReduxFramework = new ReduxFramework($this->sections, $this->args);
        }

        /**

          This is a test function that will let you see when the compiler hook occurs.
          It only runs if a field   set with compiler=>true is changed.

         * */
        function compiler_action($options, $css, $changed_values) {
            // echo '<h1>The compiler hook has run!</h1>';
            // echo "<pre>";
            // print_r($changed_values); // Values that have changed since the last save
            // echo "</pre>";
            // print_r($options); //Option values
            // print_r($css); // Compiler selector CSS values  compiler => array( CSS SELECTORS )
            

              // Demo of how to use the dynamic CSS and write your own static CSS file
              $filename = dirname(__FILE__) . '/style.min' . '.css';

              global $wp_filesystem;
              if( empty( $wp_filesystem ) ) {
                require_once( ABSPATH .'/wp-admin/includes/file.php' );
              WP_Filesystem();
              }

              if( $wp_filesystem ) {
                $wp_filesystem->put_contents(
                    $filename,
                    $css,
                    FS_CHMOD_FILE // predefined mode settings for WP files
                );
              }
             
        }

        /**

          This is a test function that will let you see when the compiler hook occurs.
          It only runs if a field   set with compiler=>true is changed.

         * */
        function compiler_action_js($options, $js, $changed_values) {
            // echo '<h1>The compiler hook has run!</h1>';
            // echo "<pre>";
            // print_r($changed_values); // Values that have changed since the last save
            // echo "</pre>";
            // print_r($options); //Option values
            // print_r($js); // Compiler selector CSS values  compiler => array( CSS SELECTORS )
            

              // Demo of how to use the dynamic CSS and write your own static CSS file
              $filename = dirname(__FILE__) . '/main.min' . '.js';

              global $wp_filesystem;
              if( empty( $wp_filesystem ) ) {
                require_once( ABSPATH .'/wp-admin/includes/file.php' );
              WP_Filesystem();
              }

              if( $wp_filesystem ) {
                $wp_filesystem->put_contents(
                    $filename,
                    $js,
                    FS_CHMOD_FILE // predefined mode settings for WP files
                );
              }
             
        }




        /**

          Custom function for filtering the sections array. Good for child themes to override or add to the sections.
          Simply include this function in the child themes functions.php file.

          NOTE: the defined constants for URLs, and directories will NOT be available at this point in a child theme,
          so you must use get_template_directory_uri() if you want to use any of the built in icons

         * */
        function dynamic_section($sections) {
            //$sections = array();
            $sections[] = array(
                'title' => __('Section via hook', 'redux-framework-demo'),
                'desc' => __('<p class="description">This is a section created by adding a filter to the sections array. Can be used by child themes to add/remove sections from the options.</p>', 'redux-framework-demo'),
                'icon' => 'el-icon-paper-clip',
                // Leave this as a blank section, no options just some intro text set above.
                'fields' => array()
            );

            return $sections;
        }

        /**

          Filter hook for filtering the args. Good for child themes to override or add to the args array. Can also be used in other functions.

         * */
        function change_arguments($args) {
            //$args['dev_mode'] = true;

            return $args;
        }

        /**

          Filter hook for filtering the default value of any given field. Very useful in development mode.

         * */
        function change_defaults($defaults) {
            $defaults['str_replace'] = 'Testing filter hook!';

            return $defaults;
        }

        // Remove the demo link and the notice of integrated demo from the redux-framework plugin
        function remove_demo() {

            // Used to hide the demo mode link from the plugin page. Only used when Redux is a plugin.
            if (class_exists('ReduxFrameworkPlugin')) {
                remove_filter('plugin_row_meta', array(ReduxFrameworkPlugin::instance(), 'plugin_metalinks'), null, 2);

                // Used to hide the activation notice informing users of the demo panel. Only used when Redux is a plugin.
                remove_action('admin_notices', array(ReduxFrameworkPlugin::instance(), 'admin_notices'));
            }
        }

        public function setSections() {

            /**
              Used within different fields. Simply examples. Search for ACTUAL DECLARATION for field examples
             * */
            // Background Patterns Reader
            $sample_patterns_path   = ReduxFramework::$_dir . '../sample/patterns/';
            $sample_patterns_url    = ReduxFramework::$_url . '../sample/patterns/';
            $sample_patterns        = array();

            if (is_dir($sample_patterns_path)) :

                if ($sample_patterns_dir = opendir($sample_patterns_path)) :
                    $sample_patterns = array();

                    while (( $sample_patterns_file = readdir($sample_patterns_dir) ) !== false) {

                        if (stristr($sample_patterns_file, '.png') !== false || stristr($sample_patterns_file, '.jpg') !== false) {
                            $name = explode('.', $sample_patterns_file);
                            $name = str_replace('.' . end($name), '', $sample_patterns_file);
                            $sample_patterns[]  = array('alt' => $name, 'img' => $sample_patterns_url . $sample_patterns_file);
                        }
                    }
                endif;
            endif;

            ob_start();

            $ct             = wp_get_theme();
            $this->theme    = $ct;
            $item_name      = $this->theme->get('Name');
            $tags           = $this->theme->Tags;
            $screenshot     = $this->theme->get_screenshot();
            $class          = $screenshot ? 'has-screenshot' : '';

            $customize_title = sprintf(__('Customize &#8220;%s&#8221;', 'redux-framework-demo'), $this->theme->display('Name'));
            
            ?>
            <div id="current-theme" class="<?php echo esc_attr($class); ?>">
            <?php if ($screenshot) : ?>
                <?php if (current_user_can('edit_theme_options')) : ?>
                        <a href="<?php echo wp_customize_url(); ?>" class="load-customize hide-if-no-customize" title="<?php echo esc_attr($customize_title); ?>">
                            <img src="<?php echo esc_url($screenshot); ?>" alt="<?php esc_attr_e('Current theme preview'); ?>" />
                        </a>
                <?php endif; ?>
                    <img class="hide-if-customize" src="<?php echo esc_url($screenshot); ?>" alt="<?php esc_attr_e('Current theme preview'); ?>" />
                <?php endif; ?>

                <h4><?php echo $this->theme->display('Name'); ?></h4>

                <div>
                    <ul class="theme-info">
                        <li><?php printf(__('By %s', 'redux-framework-demo'), $this->theme->display('Author')); ?></li>
                        <li><?php printf(__('Version %s', 'redux-framework-demo'), $this->theme->display('Version')); ?></li>
                        <li><?php echo '<strong>' . __('Tags', 'redux-framework-demo') . ':</strong> '; ?><?php printf($this->theme->display('Tags')); ?></li>
                    </ul>
                    <p class="theme-description"><?php echo $this->theme->display('Description'); ?></p>
            <?php
            if ($this->theme->parent()) {
                printf(' <p class="howto">' . __('This <a href="%1$s">child theme</a> requires its parent theme, %2$s.') . '</p>', __('http://codex.wordpress.org/Child_Themes', 'redux-framework-demo'), $this->theme->parent()->display('Name'));
            }
            ?>

                </div>
            </div>

            <?php
            $item_info = ob_get_contents();

            ob_end_clean();

            $sampleHTML = '';
            if (file_exists(dirname(__FILE__) . '/info-html.html')) {
                /** @global WP_Filesystem_Direct $wp_filesystem  */
                global $wp_filesystem;
                if (empty($wp_filesystem)) {
                    require_once(ABSPATH . '/wp-admin/includes/file.php');
                    WP_Filesystem();
                }
                $sampleHTML = $wp_filesystem->get_contents(dirname(__FILE__) . '/info-html.html');
            }


            $this->sections[] = array(
                'title'     => __('General', 'wxeo-wun'),
                'fields'    => array(

                    array(
                        'id'            => 'wxeo-general-color',
                        'type'          => 'color',
                        'title'         => __('Text Color Option', 'wxeo-wun'),
                        'active'        => false,
                        'transparent'   => false,
                        'default'       => '#444444',
                        'validate'  => 'color',
                        'css_house'     => true,
                    ),

                    array(
                        'id'        => 'wxeo-general-link-color',
                        'type'      => 'link_color',
                        'title'     => __('Links Color Option', 'wxeo-wun'),
                        'active'    => false,
                        'default'   => array(
                            'regular'  => '#444444',
                            'hover'    => '#636363',
                        ),
                        'css_house' => true,
                    ),

                    array(
                        'id'          => 'optsuf-typography',
                        'type'        => 'typography', 
                        'title'       => __('Typography', 'redux-framework-demo'),
                        'google'      => true, 
                        'font-backup' => true,
                        'output'      => array('h2.site-description'),
                        'units'       =>'px',
                        'subtitle'    => __('Typography option with each property can be called individually.', 'redux-framework-demo'),
                        'default'     => array(
                            'color'       => '#333', 
                            'font-style'  => '700', 
                            'font-family' => 'Abel', 
                            'google'      => true,
                            'font-size'   => '33px', 
                            'line-height' => '40'
                        ),
                    )


                ),
            
            );
            

            
            $this->sections[] = array(
                'title'     => __('Header', 'wxeo-wun'),
				'submenu' => true,
                // 'desc'      => __('Redux Framework was created with the developer in mind. It allows for any theme developer to have an advanced theme panel with most of the features a developer would need. For more information check out the Github repo at: <a href="https://github.com/ReduxFramework/Redux-Framework">https://github.com/ReduxFramework/Redux-Framework</a>', 'redux-framework-demo'),
                // 'icon'      => 'el-icon-home',
                // 'submenu' => false, // Setting submenu to false on a given section will hide it from the WordPress sidebar menu!
                'fields'    => array(


                    array(
                        'id'        => 'wxeo-header-style',
                        'type'      => 'image_select',
                        'title'     => __('Header Style', 'wxeo-wun'),
                        'subtitle'  => __('The style of the site header', 'wxeo-wun'),
                        'options'   => array(
                            'full-bar'  => array('alt' => 'Full Bar',        'img' => 'http://placehold.it/160x50/cccccc/919191.jpg&text=FULL%20BAR'),
                            'bar'       => array('alt' => 'Bar',        'img' => 'http://placehold.it/160x50/cccccc/919191.jpg&text=BAR'),
                            'two-tier'  => array('alt' => 'Two Tier',   'img' => 'http://placehold.it/160x50/cccccc/919191.jpg&text=TWO%20TIER'),
                            'side-line' => array('alt' => 'Side Line',  'img' => 'http://placehold.it/160x50/cccccc/919191.jpg&text=SIDE%20LINE')
                        ),
                        'default'   => 'full-bar',
                        'css_house' => true,
                        'js_house'  => true,
                    ),


                    array(
                        'id'        => 'wxeo-header-size',
                        'type'      => 'image_select',
                        'title'     => __('Header Size', 'wxeo-wun'),
                        'subtitle'  => __('The height of the header bar', 'redux-framework-demo'),
                        'options'   => array(
                            'sm' => array('alt' => 'Small',        'img' => 'http://placehold.it/160x50/cccccc/919191.jpg&text=SMALL'),
                            'md' => array('alt' => 'Medium',   'img' => 'http://placehold.it/160x50/cccccc/919191.jpg&text=MEDIUM'),
                            'lg' => array('alt' => 'Large',  'img' => 'http://placehold.it/160x50/cccccc/919191.jpg&text=LARGE')
                        ),
                        'default' => 'sm',
                        'indent'    => true,
                        'required'  => array('wxeo-header-style', "!=", 'side-line'),
                    ),

                    array(
                        'id'        => 'wxeo-header-logo-position',
                        'type'      => 'image_select',
                        'title'     => __('Logo Position', 'wxeo-wun'),
                        'subtitle'  => __('Position of Logo within the header', 'wxeo-wun'),
                        'options'   => array(
                            'lt' => array('alt' => 'Left',        'img' => ReduxFramework::$_url . 'assets/img/2cl.png'),
                            // 'cn' => array('alt' => 'Center',   'img' => ReduxFramework::$_url . 'assets/img/1col.png'),
                            'rt' => array('alt' => 'Right',  'img' => ReduxFramework::$_url . 'assets/img/2cr.png')
                        ),
                        'default' => 'lt',
                        'indent'    => true,
                        'required'  => array('wxeo-header-style', "!=", 'side-line'),
                    ),

                    array(
                        'id'        => 'wxeo-header-menu-center',
                        'type'      => 'switch',
                        'title'     => __('Center Text Menus', 'wxeo-wun'),
                        'subtitle'  => __('Disable to allow left / right options', 'wxeo-wun'),
                        'default' => false,
                    ),

                    array(
                        'id'        => 'wxeo-header-main-menu-position',
                        'type'      => 'image_select',
                        'title'     => __('Main Menu Position', 'wxeo-wun'),
                        'subtitle'  => __('The positon of the main menu in the header.', 'wxeo-wun'),
                        'options'   => array(
                            'lt' => array('alt' => 'Left',        'img' => 'http://placehold.it/160x50/cccccc/919191.jpg&text=LEFT'),
                            'rt' => array('alt' => 'Right',  'img' => 'http://placehold.it/160x50/cccccc/919191.jpg&text=RIGHT')
                        ),
                        'default' => 'lt',
                        'indent'    => true,
                        'required'  => array(
                            array('wxeo-header-menu-center', "=", 0),
                            array('wxeo-header-style', "!=", 'side-line'),
                        )
                    ),


                    array(
                        'id'        => 'wxeo-header-mini-menu-position',
                        'type'      => 'image_select',
                        'title'     => __('Mini Menu Position', 'wxeo-wun'),
                        'subtitle'  => __('The positon of the mini menu in the header.', 'wxeo-wun'),
                        'options'   => array(
                            'lt' => array('alt' => 'Left',        'img' => 'http://placehold.it/160x50/cccccc/919191.jpg&text=LEFT'),
                            'rt' => array('alt' => 'Right',  'img' => 'http://placehold.it/160x50/cccccc/919191.jpg&text=RIGHT')
                        ),
                        'default' => 'rt',
                        'indent'    => true,
                        'required'  => array(
                            array('wxeo-header-menu-center', "=", 0),
                            array('wxeo-header-style', "!=", 'side-line'),
                        )
                    ),

                    array(
                        'id'        => 'wxeo-header-btn-style',
                        'type'      => 'image_select',
                        'title'     => __('Header Button Style', 'wxeo-wun'),
                        'subtitle'  => __('The style of the buttons in the main Menu', 'wxeo-wun'),
                        'options'   => array(
                            'bttn-top-line' => array('alt' => 'Top Line',        'img' => 'http://placehold.it/160x50/cccccc/919191.jpg&text=top-line'),
                            'bttn-bottom-line' => array('alt' => 'Bottom Line',   'img' => 'http://placehold.it/160x50/cccccc/919191.jpg&text=bottom-line'),
                            'bttn-txt' => array('alt' => 'Text',  'img' => 'http://placehold.it/160x50/cccccc/919191.jpg&text=txt'),
                            'bttn-border' => array('alt' => 'Border',  'img' => 'http://placehold.it/160x50/cccccc/919191.jpg&text=border'),
                            'bttn-box-border-bottom' => array('alt' => 'Border Bottom',  'img' => 'http://placehold.it/160x50/cccccc/919191.jpg&text=border-bottom'),
                            'bttn-box-border-top' => array('alt' => 'Border Top',  'img' => 'http://placehold.it/160x50/cccccc/919191.jpg&text=border-top'),
                            'bttn-box-border-left' => array('alt' => 'Border Left',  'img' => 'http://placehold.it/160x50/cccccc/919191.jpg&text=border-left'),
                            'bttn-box-border-right' => array('alt' => 'Border Right',  'img' => 'http://placehold.it/160x50/cccccc/919191.jpg&text=border-right'),
                        ),
                        'default' => 'bttn-border'
                    ),
                    
                    array(
                        'id'        => 'wxeo-header-btn-size',
                        'type'      => 'image_select',
                        'title'     => __('Header Button Size', 'wxeo-wun'),
                        'subtitle'  => __('The size of the buttons in the header', 'wxeo-wun'),
                        'options'   => array(
                            'sm' => array('alt' => 'Small',        'img' => 'http://placehold.it/160x50/cccccc/919191.jpg&text=SMALL'),
                            'md' => array('alt' => 'Medium',   'img' => 'http://placehold.it/160x50/cccccc/919191.jpg&text=MEDIUM'),
                            'lg' => array('alt' => 'Large',  'img' => 'http://placehold.it/160x50/cccccc/919191.jpg&text=LARGE')
                        ),
                        'default' => 'sm',
                        'required'  => array('wxeo-header-style', "!=", 'two-tier'),
                        
                    ),


                    array(
                        'id'        => 'wxeo-header-btn-size-two-tier',
                        'type'      => 'image_select',
                        'title'     => __('Header Button Size', 'wxeo-wun'),
                        'subtitle'  => __('The size of the buttons in the header', 'wxeo-wun'),
                        'options'   => array(
                            'sm' => array('alt' => 'Small',        'img' => 'http://placehold.it/160x50/cccccc/919191.jpg&text=SMALL'),
                            'md' => array('alt' => 'Medium',   'img' => 'http://placehold.it/160x50/cccccc/919191.jpg&text=MEDIUM'),
                        ),
                        'default' => 'sm',
                        'required'  => array('wxeo-header-style', "=", 'two-tier'),
                        
                    ),

                    array(
                        'id'        => 'wxeo-header-btn-spacing',
                        'type'      => 'image_select',
                        'title'     => __('Header Button Spacing', 'wxeo-wun'),
                        'subtitle'  => __('The spacing between the buttons in the header', 'wxeo-wun'),
                        'options'   => array(
                            'na' => array('alt' => 'No Spacing','img' => 'http://placehold.it/160x50/cccccc/919191.jpg&text=NONE'),
                            'sm' => array('alt' => 'Small',     'img' => 'http://placehold.it/160x50/cccccc/919191.jpg&text=SMALL'),
                            'md' => array('alt' => 'Medium',    'img' => 'http://placehold.it/160x50/cccccc/919191.jpg&text=MEDIUM'),
                            'lg' => array('alt' => 'Large',     'img' => 'http://placehold.it/160x50/cccccc/919191.jpg&text=LARGE'),
                        ),
                        'default' => 'sm',
                    ),



                    array(
                        'id'        => 'wxeo-header-search',
                        'type'      => 'image_select',
                        'title'     => __('Header Search Style', 'wxeo-wun'),
                        'options'   => array(
                            'na'            => array('alt' => 'No Search','img' => 'http://placehold.it/160x50/cccccc/919191.jpg&text=NONE'),
                            'full-bar'      => array('alt' => 'Small',    'img' => 'http://placehold.it/160x50/cccccc/919191.jpg&text=FULL%20BAR'),
                            'click-hover'   => array('alt' => 'Large',    'img' => 'http://placehold.it/160x50/cccccc/919191.jpg&text=CLICK%20HOVER'),
                            'full-search'   => array('alt' => 'Large',    'img' => 'http://placehold.it/160x50/cccccc/919191.jpg&text=FULL%20SEARCH'),
                        ),
                        'default' => 'na',
                        'css_house' => true,
                        'js_house'  => true,
                    ),

                ),
                    
            );

			// Social URL code start
			$this->sections[] = array(
                'title'     => __('Social URL', 'wxeo-wun'),
                'subsection' => true,
                'fields'    => array(

                    array(
                        'id'        => 'wxeo-social-style',
                        'type'      => 'image_select',
                        'title'     => __('Social Icon Style', 'wxeo-wun'),
                        'options'   => array(
                            'box' => array('alt' => 'Boxed',        'img' => 'http://placehold.it/160x50/cccccc/919191.jpg&text=BOXED'),
                            'full' => array('alt' => 'Full Height',   'img' => 'http://placehold.it/160x50/cccccc/919191.jpg&text=FULL+HEIGHT'),
                        ),
                        'default' => 'box',
                        
                    ),


                    array(
                        'id'        => 'wxeo-social-facebook-url',
                        'type'      => 'text',
                        'title'     => __('Facebook URL', 'wxeo-wun'),
                        'css_house' => true,
                     
                    ),
					array(
                        'id'        => 'wxeo-social-twitter-url',
                        'type'      => 'text',
                        'title'     => __('<i class="fa fa-twitter"></i>Twitter URL', 'wxeo-wun'),
                        'css_house' => true,
                     
                    ),
					array(
                        'id'        => 'wxeo-social-google-url',
                        'type'      => 'text',
                        'title'     => __('<i class="fa fa-deviantart"></i>Google URL', 'wxeo-wun'),
                        'css_house' => true,
                     
                    ),
					array(
                        'id'        => 'wxeo-social-flickr-url',
                        'type'      => 'text',
                        'title'     => __('<i class="fa fa-flickr"></i>Flickr URL', 'wxeo-wun'),
                        'css_house' => true,
                     
                    ),
					array(
                        'id'        => 'wxeo-social-skype-url',
                        'type'      => 'text',
                        'title'     => __('<i class="fa fa-skype"></i>Skype ID', 'wxeo-wun'),
                  
                        'css_house' => true,
                     
                    ),
					array(
                        'id'        => 'wxeo-social-tumblr-url',
                        'type'      => 'text',
                        'title'     => __('<i class="fa fa-tumblr"></i>Tumblr URL', 'wxeo-wun'),
                        'css_house' => true,
                     
                    ),
					array(
                        'id'        => 'wxeo-social-youtube-url',
                        'type'      => 'text',
                        'title'     => __('<i class="fa fa-youtube-square"></i>Youtube URL', 'wxeo-wun'),
                        'css_house' => true,
                     
                    ),
					array(
                        'id'        => 'wxeo-social-github-url',
                        'type'      => 'text',
                        'title'     => __('<i class="fa fa-github"></i>Github URL', 'wxeo-wun'),
                      
                        'css_house' => true,
                     
                    ),array(
                        'id'        => 'wxeo-social-dribbble-url',
                        'type'      => 'text',
                        'title'     => __('<i class="fa fa-dribbble"></i>Dribbble URL', 'wxeo-wun'),
                      
                        'css_house' => true,
                     
                    ),array(
                        'id'        => 'wxeo-social-instagram-url',
                        'type'      => 'text',
                        'title'     => __('<i class="fa fa-instagram"></i>Instagram URL', 'wxeo-wun'),
                      
                        'css_house' => true,
                     
                    ),array(
                        'id'        => 'wxeo-social-pinterest-url',
                        'type'      => 'text',
                        'title'     => __('<i class="fa fa-pinterest"></i>Pinterest URL', 'wxeo-wun'),
                      
                        'css_house' => true,
                     
                    ),
					array(
                        'id'        => 'wxeo-social-linkedin-url',
                        'type'      => 'text',
                        'title'     => __('<i class="fa fa-linkedin"></i>Linkedin URL', 'wxeo-wun'),
                    
                        'css_house' => true,
                     
                    )
				)
            );
			
			
            $this->sections[] = array(
                'title'     => __('Footer', 'wxeo-wun'),
                'submenu' => true,
                // 'desc'      => __('Redux Framework was created with the developer in mind. It allows for any theme developer to have an advanced theme panel with most of the features a developer would need. For more information check out the Github repo at: <a href="https://github.com/ReduxFramework/Redux-Framework">https://github.com/ReduxFramework/Redux-Framework</a>', 'redux-framework-demo'),
                // 'icon'      => 'el-icon-home',
                // 'submenu' => false, // Setting submenu to false on a given section will hide it from the WordPress sidebar menu!
                'fields'    => array(


                    array(
                        'id'        => 'wxeo-footer-style',
                        'type'      => 'image_select',
                        'title'     => __('Footer Style', 'wxeo-wun'),
                        'subtitle'  => __('The style of the site footer', 'wxeo-wun'),
                        'options'   => array(
                                'one-column'  => array('alt' => 'One Column',        'img' => 'http://placehold.it/160x50/cccccc/919191.jpg&text=ONE+COLUMN'),
                                'two-column'       => array('alt' => 'Two Column',        'img' => 'http://placehold.it/160x50/cccccc/919191.jpg&text=TWO+COLUMN'),
                                'three-column'  => array('alt' => 'Three Column',   'img' => 'http://placehold.it/160x50/cccccc/919191.jpg&text=THREE+COLUMN'),
                                'four-column' => array('alt' => 'Four Column',  'img' => 'http://placehold.it/160x50/cccccc/919191.jpg&text=FOUR+COLUMN')
                            ),
                        'default'   => 'one-column',
                        'css_house' => true,
                        'js_house'  => true,
                    ),
                ),
                    
            ); 
			
			


            $this->sections[] = array(
                'title'     => __('Shortcodes', 'wxeo-wun'),
                'submenu' => true,
                'fields'    => array(


                ),
                    
            );


            $this->sections[] = array(
                'title'     => __('Milestone Counter', 'wxeo-wun'),
                'subsection' => true,
                'fields'    => array(

                    array(
                        'id'        => 'wxeo-shortcode-milestone-icon-background',
                        'type'      => 'image_select',
                        'title'     => __('Icon Style', 'wxeo-wun'),
                        'subtitle'  => __('Milestone icon style type.', 'wxeo-wun'),
                        'options'   => array(
                            'na'    => array('alt' => 'No Background',      'img' => 'http://placehold.it/160x50/cccccc/919191.jpg&text=NO%20BACKGROUND'),
                            'color' => array('alt' => 'Color Background',   'img' => 'http://placehold.it/160x50/cccccc/919191.jpg&text=BACKGROUND'),
                        ),
                        'default'   => 'na',
                        'css_house' => true,
                        // 'js_house'  => true,
                    ),

                    array(
                        'id'        => 'wxeo-shortcode-milestone-icon-color',
                        'type'      => 'color',
                        'title'     => __('Icon Color', 'wxeo-wun'),
                        'subtitle'  => __('Milestone icon color.', 'wxeo-wun'),
                        'default'   => '#9B75E6',
                        'validate'  => 'color',
                        'transparent', false,
                        'css_house' => true,
                    ),

                    array(
                        'id'        => 'wxeo-shortcode-milestone-background-color',
                        'type'      => 'color',
                        'title'     => __('Icon Background Color', 'wxeo-wun'),
                        'subtitle'  => __('Milestone icon background color.', 'wxeo-wun'),
                        'default'   => '#A6DA27',
                        'validate'  => 'color',
                        'transparent', false,
                        'css_house' => true,
                        'required'  => array('wxeo-shortcode-milestone-icon-background', "=", 'color'),
                    ),


                    array(
                        'id'        => 'wxeo-shortcode-milestone-icon-border',
                        'type'      => 'border',
                        'title'     => __('Icon Border', 'wxeo-wun'),
                        'all'       => false,
                        'default'   => array(
                            'border-color'  => '#E5E5E5', 
                            'border-style'  => 'solid', 
                            'border-top'    => '4px',
                            'border-right'  => '4px',
                            'border-bottom' => '4px',
                            'border-left'   => '4px',
                        ),
                        'css_house' => true,
                        'required'  => array('wxeo-shortcode-milestone-icon-background', "=", 'color'),
                    ),

                    array(
                        'id'        => 'wxeo-shortcode-milestone-icon-radius',
                        'type'      => 'image_select',
                        'title'     => __('Milestone Position', 'wxeo-wun'),
                        'options'   => array(
                            '0px' => array('alt' => 'Squared',  'img' => 'http://placehold.it/160x50/cccccc/919191.jpg&text=0PX'),
                            '5px' => array('alt' => 'Small Radius (5px)', 'img' => 'http://placehold.it/160x50/cccccc/919191.jpg&text=5PX'),
                            '10px' => array('alt' => 'Medium Radius (10px)','img' => 'http://placehold.it/160x50/cccccc/919191.jpg&text=10PX'),
                            '500px' => array('alt' => 'Large Radius (500px)','img' => 'http://placehold.it/160x50/cccccc/919191.jpg&text=500PX'),
                        ),
                        'default'   => '5px',
                        'css_house' => true,
                        'required'  => array('wxeo-shortcode-milestone-icon-background', "=", 'color'),
                    ),

                    array(
                        'id'        => 'wxeo-shortcode-milestone-position',
                        'type'      => 'image_select',
                        'title'     => __('Milestone Position', 'wxeo-wun'),
                        'options'   => array(
                            'cn' => array('alt' => 'Centered',  'img' => 'http://placehold.it/160x50/cccccc/919191.jpg&text=CENTERED'),
                            'lt' => array('alt' => 'Icon Left', 'img' => 'http://placehold.it/160x50/cccccc/919191.jpg&text=ICON%20LEFT'),
                            'rt' => array('alt' => 'Icon Right','img' => 'http://placehold.it/160x50/cccccc/919191.jpg&text=ICON%20RIGHT'),
                        ),
                        'default'   => 'cn',
                        'css_house' => true,
                        // 'js_house'  => true,
                    ),





                    array(
                        'id'            => 'wxeo-shortcode-milestone-counter-font',
                        'type'          => 'typography',
                        'title'         => __('Counter Typography', 'wxeo-wun'),
                        'google'        => true,
                        'font-backup'   => true,
                        'word-spacing'  => true,
                        'letter-spacing'=> true,
                        'all_styles'    => true,
                        'units'         => 'px',
                        'subtitle'      => __('Typography options for the counter.', 'wxeo-wun'),
                        // 'compiler' => true,
                        'default'       => array(
                            'color'         => '#333',
                            'font-style'    => '700',
                            'font-family'   => 'Abel',
                            'google'        => true,
                            'font-size'     => '33px',
                            'line-height'   => '40px'),
                    ),

                    array(
                        'id'            => 'wxeo-shortcode-milestone-subtitle-font',
                        'type'          => 'typography',
                        'title'         => __('Subtitle Typography', 'wxeo-wun'),
                        'google'        => true,
                        'font-backup'   => true,
                        'word-spacing'  => true,
                        'letter-spacing'=> true,
                        'all_styles'    => true,
                        'units'         => 'px',
                        'subtitle'      => __('Typography options for the subtitle.', 'wxeo-wun'),
                        'default'       => array(
                            'color'         => '#333',
                            'font-style'    => '700',
                            'font-family'   => 'Abel',
                            'google'        => true,
                            'font-size'     => '33px',
                            'line-height'   => '40px'),
                    ),

                    

                ),
                    
            );






            $this->sections[] = array(
                'title'     => __('Feature List', 'wxeo-wun'),
                'subsection' => true,
                'fields'    => array(

                    array(
                        'id'        => 'wxeo-shortcode-feature-list-on-off',
                        'type'      => 'switch',
                        'title'     => __('Feature List Status', 'wxeo-wun'),
                        'subtitle'  => __('If turned off the css for featured list wont be compiled saving space.', 'wxeo-wun'),
                        'default'   => true,
                        'on'        => 'Enable',
                        'off'       => 'Disable',
                        'css_house' => true,
                    ),


                    array(
                        'id'        => 'wxeo-shortcode-feature-list-icon-background',
                        'type'      => 'image_select',
                        'title'     => __('Icon Style', 'wxeo-wun'),
                        'subtitle'  => __('Milestone icon style type.', 'wxeo-wun'),
                        'options'   => array(
                            'na'    => array('alt' => 'No Background',      'img' => 'http://placehold.it/160x50/cccccc/919191.jpg&text=NO%20BACKGROUND'),
                            'color' => array('alt' => 'Color Background',   'img' => 'http://placehold.it/160x50/cccccc/919191.jpg&text=BACKGROUND'),
                        ),
                        'default'   => 'na',
                        'css_house' => true,
                        // 'js_house'  => true,
                        'required'  => array('wxeo-shortcode-feature-list-on-off', "=", true),
                    ),

                    array(
                        'id'        => 'wxeo-shortcode-feature-list-icon-color',
                        'type'      => 'color',
                        'title'     => __('Icon Color', 'wxeo-wun'),
                        'subtitle'  => __('Milestone icon color.', 'wxeo-wun'),
                        'default'   => '#909090',
                        'validate'  => 'color',
                        'transparent', false,
                        'css_house' => true,
                        'required'  => array('wxeo-shortcode-feature-list-on-off', "=", true),
                    ),

                    array(
                        'id'        => 'wxeo-shortcode-feature-list-background-color',
                        'type'      => 'color',
                        'title'     => __('Icon Background Color', 'wxeo-wun'),
                        'subtitle'  => __('Milestone icon background color.', 'wxeo-wun'),
                        'default'   => '#F1F1F1',
                        'validate'  => 'color',
                        'transparent', false,
                        'css_house' => true,
                        'required'  => array(
                            array('wxeo-shortcode-feature-list-icon-background', "=", 'color'),
                            array('wxeo-shortcode-feature-list-on-off', "=", true),
                        ),
                    ),


                    array(
                        'id'        => 'wxeo-shortcode-feature-list-icon-border',
                        'type'      => 'border',
                        'title'     => __('Icon Border', 'wxeo-wun'),
                        'all'       => false,
                        'default'   => array(
                            'border-color'  => '#E5E5E5', 
                            'border-style'  => 'solid', 
                            'border-top'    => '1px',
                            'border-right'  => '1px',
                            'border-bottom' => '1px',
                            'border-left'   => '1px',
                        ),
                        'css_house' => true,
                        'required'  => array(
                            array('wxeo-shortcode-feature-list-icon-background', "=", 'color'),
                            array('wxeo-shortcode-feature-list-on-off', "=", true),
                        ),
                    ),

                    array(
                        'id'        => 'wxeo-shortcode-feature-list-icon-radius',
                        'type'      => 'image_select',
                        'title'     => __('Milestone Position', 'wxeo-wun'),
                        'options'   => array(
                            '0px' => array('alt' => 'Squared',  'img' => 'http://placehold.it/160x50/cccccc/919191.jpg&text=0PX'),
                            '5px' => array('alt' => 'Small Radius (5px)', 'img' => 'http://placehold.it/160x50/cccccc/919191.jpg&text=5PX'),
                            '10px' => array('alt' => 'Medium Radius (10px)','img' => 'http://placehold.it/160x50/cccccc/919191.jpg&text=10PX'),
                            '500px' => array('alt' => 'Large Radius (500px)','img' => 'http://placehold.it/160x50/cccccc/919191.jpg&text=500PX'),
                        ),
                        'default'   => '0px',
                        'css_house' => true,
                        'required'  => array('wxeo-shortcode-feature-list-icon-background', "=", 'color'),
                    ),

                    array(
                        'id'        => 'wxeo-shortcode-feature-list-position',
                        'type'      => 'image_select',
                        'title'     => __('Milestone Position', 'wxeo-wun'),
                        'options'   => array(
                            'cn' => array('alt' => 'Centered',  'img' => 'http://placehold.it/160x50/cccccc/919191.jpg&text=CENTERED'),
                            'lt' => array('alt' => 'Icon Left', 'img' => 'http://placehold.it/160x50/cccccc/919191.jpg&text=ICON%20LEFT'),
                            'rt' => array('alt' => 'Icon Right','img' => 'http://placehold.it/160x50/cccccc/919191.jpg&text=ICON%20RIGHT'),
                        ),
                        'default'   => 'cn',
                        'css_house' => true,
                        // 'js_house'  => true,
                        'required'  => array('wxeo-shortcode-feature-list-on-off', "=", true),
                    ),





                    array(
                        'id'            => 'wxeo-shortcode-feature-list-counter-font',
                        'type'          => 'typography',
                        'title'         => __('Counter Typography', 'wxeo-wun'),
                        'google'        => true,
                        'font-backup'   => true,
                        'word-spacing'  => true,
                        'letter-spacing'=> true,
                        'all_styles'    => true,
                        'units'         => 'px',
                        'subtitle'      => __('Typography options for the counter.', 'wxeo-wun'),
                        // 'compiler' => true,
                        'default'       => array(
                            'color'         => '#333',
                            'font-style'    => '700',
                            'font-family'   => 'Abel',
                            'google'        => true,
                            'font-size'     => '33px',
                            'line-height'   => '40px'
                        ),
                        'required'  => array('wxeo-shortcode-feature-list-on-off', "=", true),
                    ),

                    array(
                        'id'            => 'wxeo-shortcode-feature-list-subtitle-font',
                        'type'          => 'typography',
                        'title'         => __('Subtitle Typography', 'wxeo-wun'),
                        'google'        => true,
                        'font-backup'   => true,
                        'word-spacing'  => true,
                        'letter-spacing'=> true,
                        'all_styles'    => true,
                        'units'         => 'px',
                        'subtitle'      => __('Typography options for the subtitle.', 'wxeo-wun'),
                        'default'       => array(
                            'color'         => '#333',
                            'font-style'    => '700',
                            'font-family'   => 'Abel',
                            'google'        => true,
                            'font-size'     => '33px',
                            'line-height'   => '40px'
                        ),
                        'required'  => array('wxeo-shortcode-feature-list-on-off', "=", true),
                    ),

                    

                ),
                    
            );


            $this->sections[] = array(
                'title'     => __('Feature Inline List', 'wxeo-wun'),
                'subsection' => true,
                'fields'    => array(

                    array(
                        'id'        => 'wxeo-shortcode-feature-inline-list-on-off',
                        'type'      => 'switch',
                        'title'     => __('Feature List Status', 'wxeo-wun'),
                        'subtitle'  => __('If turned off the css for featured list wont be compiled saving space.', 'wxeo-wun'),
                        'default'   => true,
                        'on'        => 'Enable',
                        'off'       => 'Disable',
                        'css_house' => true,
                    ),


                    array(
                        'id'        => 'wxeo-shortcode-feature-inline-list-icon-background',
                        'type'      => 'image_select',
                        'title'     => __('Icon Style', 'wxeo-wun'),
                        'subtitle'  => __('Milestone icon style type.', 'wxeo-wun'),
                        'options'   => array(
                            'na'    => array('alt' => 'No Background',      'img' => 'http://placehold.it/160x50/cccccc/919191.jpg&text=NO%20BACKGROUND'),
                            'color' => array('alt' => 'Color Background',   'img' => 'http://placehold.it/160x50/cccccc/919191.jpg&text=BACKGROUND'),
                        ),
                        'default'   => 'na',
                        'css_house' => true,
                        // 'js_house'  => true,
                        'required'  => array('wxeo-shortcode-feature-inline-list-on-off', "=", true),
                    ),

                    array(
                        'id'        => 'wxeo-shortcode-feature-inline-list-icon-color',
                        'type'      => 'color',
                        'title'     => __('Icon Color', 'wxeo-wun'),
                        'subtitle'  => __('Milestone icon color.', 'wxeo-wun'),
                        'default'   => '#909090',
                        'validate'  => 'color',
                        'transparent', false,
                        'css_house' => true,
                        'required'  => array('wxeo-shortcode-feature-inline-list-on-off', "=", true),
                    ),

                    array(
                        'id'        => 'wxeo-shortcode-feature-inline-list-background-color',
                        'type'      => 'color',
                        'title'     => __('Icon Background Color', 'wxeo-wun'),
                        'subtitle'  => __('Milestone icon background color.', 'wxeo-wun'),
                        'default'   => '#F1F1F1',
                        'validate'  => 'color',
                        'transparent', false,
                        'css_house' => true,
                        'required'  => array(
                            array('wxeo-shortcode-feature-inline-list-icon-background', "=", 'color'),
                            array('wxeo-shortcode-feature-inline-list-on-off', "=", true),
                        ),
                    ),


                    array(
                        'id'        => 'wxeo-shortcode-feature-inline-list-icon-border',
                        'type'      => 'border',
                        'title'     => __('Icon Border', 'wxeo-wun'),
                        'all'       => false,
                        'default'   => array(
                            'border-color'  => '#E5E5E5', 
                            'border-style'  => 'solid', 
                            'border-top'    => '1px',
                            'border-right'  => '1px',
                            'border-bottom' => '1px',
                            'border-left'   => '1px',
                        ),
                        'css_house' => true,
                        'required'  => array(
                            array('wxeo-shortcode-feature-inline-list-icon-background', "=", 'color'),
                            array('wxeo-shortcode-feature-inline-list-on-off', "=", true),
                        ),
                    ),

                    array(
                        'id'        => 'wxeo-shortcode-feature-inline-list-icon-radius',
                        'type'      => 'image_select',
                        'title'     => __('Milestone Position', 'wxeo-wun'),
                        'options'   => array(
                            '0px' => array('alt' => 'Squared',  'img' => 'http://placehold.it/160x50/cccccc/919191.jpg&text=0PX'),
                            '5px' => array('alt' => 'Small Radius (5px)', 'img' => 'http://placehold.it/160x50/cccccc/919191.jpg&text=5PX'),
                            '10px' => array('alt' => 'Medium Radius (10px)','img' => 'http://placehold.it/160x50/cccccc/919191.jpg&text=10PX'),
                            '500px' => array('alt' => 'Large Radius (500px)','img' => 'http://placehold.it/160x50/cccccc/919191.jpg&text=500PX'),
                        ),
                        'default'   => '0px',
                        'css_house' => true,
                        'required'  => array('wxeo-shortcode-feature-inline-list-icon-background', "=", 'color'),
                    ),

                    array(
                        'id'        => 'wxeo-shortcode-feature-inline-list-position',
                        'type'      => 'image_select',
                        'title'     => __('Milestone Position', 'wxeo-wun'),
                        'options'   => array(
                            'cn' => array('alt' => 'Centered',  'img' => 'http://placehold.it/160x50/cccccc/919191.jpg&text=CENTERED'),
                            'lt' => array('alt' => 'Icon Left', 'img' => 'http://placehold.it/160x50/cccccc/919191.jpg&text=ICON%20LEFT'),
                            'rt' => array('alt' => 'Icon Right','img' => 'http://placehold.it/160x50/cccccc/919191.jpg&text=ICON%20RIGHT'),
                        ),
                        'default'   => 'cn',
                        // 'js_house'  => true,
                        'required'  => array('wxeo-shortcode-feature-inline-list-on-off', "=", true),
                    ),





                    array(
                        'id'            => 'wxeo-shortcode-feature-inline-list-counter-font',
                        'type'          => 'typography',
                        'title'         => __('Counter Typography', 'wxeo-wun'),
                        'google'        => true,
                        'font-backup'   => true,
                        'word-spacing'  => true,
                        'letter-spacing'=> true,
                        'all_styles'    => true,
                        'units'         => 'px',
                        'subtitle'      => __('Typography options for the counter.', 'wxeo-wun'),
                        // 'compiler' => true,
                        'default'       => array(
                            'color'         => '#333',
                            'font-style'    => '700',
                            'font-family'   => 'Abel',
                            'google'        => true,
                            'font-size'     => '33px',
                            'line-height'   => '40px'
                        ),
                        'required'  => array('wxeo-shortcode-feature-inline-list-on-off', "=", true),
                    ),

                    array(
                        'id'            => 'wxeo-shortcode-feature-inline-list-subtitle-font',
                        'type'          => 'typography',
                        'title'         => __('Subtitle Typography', 'wxeo-wun'),
                        'google'        => true,
                        'font-backup'   => true,
                        'word-spacing'  => true,
                        'letter-spacing'=> true,
                        'all_styles'    => true,
                        'units'         => 'px',
                        'subtitle'      => __('Typography options for the subtitle.', 'wxeo-wun'),
                        'default'       => array(
                            'color'         => '#333',
                            'font-style'    => '700',
                            'font-family'   => 'Abel',
                            'google'        => true,
                            'font-size'     => '33px',
                            'line-height'   => '40px'
                        ),
                        'required'  => array('wxeo-shortcode-feature-inline-list-on-off', "=", true),
                    ),

                    

                ),
                    
            );

            

            $this->sections[] = array(
                'title'     => __('Feature List Centered', 'wxeo-wun'),
                'subsection' => true,
                'fields'    => array(

                    array(
                        'id'        => 'wxeo-shortcode-feature-centered-list-on-off',
                        'type'      => 'switch',
                        'title'     => __('Feature List Status', 'wxeo-wun'),
                        'subtitle'  => __('If turned off the css for featured list wont be compiled saving space.', 'wxeo-wun'),
                        'default'   => true,
                        'on'        => 'Enable',
                        'off'       => 'Disable',
                        'css_house' => true,
                    ),


                    array(
                        'id'        => 'wxeo-shortcode-feature-centered-list-icon-background',
                        'type'      => 'image_select',
                        'title'     => __('Icon Style', 'wxeo-wun'),
                        'subtitle'  => __('Milestone icon style type.', 'wxeo-wun'),
                        'options'   => array(
                            'na'    => array('alt' => 'No Background',      'img' => 'http://placehold.it/160x50/cccccc/919191.jpg&text=NO%20BACKGROUND'),
                            'color' => array('alt' => 'Color Background',   'img' => 'http://placehold.it/160x50/cccccc/919191.jpg&text=BACKGROUND'),
                        ),
                        'default'   => 'na',
                        'css_house' => true,
                        // 'js_house'  => true,
                        'required'  => array('wxeo-shortcode-feature-centered-list-on-off', "=", true),
                    ),

                    array(
                        'id'        => 'wxeo-shortcode-feature-centered-list-icon-color',
                        'type'      => 'color',
                        'title'     => __('Icon Color', 'wxeo-wun'),
                        'subtitle'  => __('Milestone icon color.', 'wxeo-wun'),
                        'default'   => '#909090',
                        'validate'  => 'color',
                        'transparent', false,
                        'css_house' => true,
                        'required'  => array('wxeo-shortcode-feature-centered-list-on-off', "=", true),
                    ),

                    array(
                        'id'        => 'wxeo-shortcode-feature-centered-list-background-color',
                        'type'      => 'color',
                        'title'     => __('Icon Background Color', 'wxeo-wun'),
                        'subtitle'  => __('Milestone icon background color.', 'wxeo-wun'),
                        'default'   => '#F1F1F1',
                        'validate'  => 'color',
                        'transparent', false,
                        'css_house' => true,
                        'required'  => array(
                            array('wxeo-shortcode-feature-centered-list-icon-background', "=", 'color'),
                            array('wxeo-shortcode-feature-centered-list-on-off', "=", true),
                        ),
                    ),


                    array(
                        'id'        => 'wxeo-shortcode-feature-centered-list-icon-border',
                        'type'      => 'border',
                        'title'     => __('Icon Border', 'wxeo-wun'),
                        'all'       => false,
                        'default'   => array(
                            'border-color'  => '#E5E5E5', 
                            'border-style'  => 'solid', 
                            'border-top'    => '1px',
                            'border-right'  => '1px',
                            'border-bottom' => '1px',
                            'border-left'   => '1px',
                        ),
                        'css_house' => true,
                        'required'  => array(
                            array('wxeo-shortcode-feature-centered-list-icon-background', "=", 'color'),
                            array('wxeo-shortcode-feature-centered-list-on-off', "=", true),
                        ),
                    ),

                    array(
                        'id'        => 'wxeo-shortcode-feature-centered-list-icon-radius',
                        'type'      => 'image_select',
                        'title'     => __('Featured Style', 'wxeo-wun'),
                        'options'   => array(
                            '0px' => array('alt' => 'Squared',  'img' => 'http://placehold.it/160x50/cccccc/919191.jpg&text=0PX'),
                            '5px' => array('alt' => 'Small Radius (5px)', 'img' => 'http://placehold.it/160x50/cccccc/919191.jpg&text=5PX'),
                            '10px' => array('alt' => 'Medium Radius (10px)','img' => 'http://placehold.it/160x50/cccccc/919191.jpg&text=10PX'),
                            '500px' => array('alt' => 'Large Radius (500px)','img' => 'http://placehold.it/160x50/cccccc/919191.jpg&text=500PX'),
                        ),
                        'default'   => '0px',
                        'css_house' => true,
                        'required'  => array('wxeo-shortcode-feature-centered-list-icon-background', "=", 'color'),
                    ),


                    array(
                        'id'            => 'wxeo-shortcode-feature-centered-list-counter-font',
                        'type'          => 'typography',
                        'title'         => __('Counter Typography', 'wxeo-wun'),
                        'google'        => true,
                        'font-backup'   => true,
                        'word-spacing'  => true,
                        'letter-spacing'=> true,
                        'all_styles'    => true,
                        'units'         => 'px',
                        'subtitle'      => __('Typography options for the counter.', 'wxeo-wun'),
                        // 'compiler' => true,
                        'default'       => array(
                            'color'         => '#333',
                            'font-style'    => '700',
                            'font-family'   => 'Abel',
                            'google'        => true,
                            'font-size'     => '33px',
                            'line-height'   => '40px'
                        ),
                        'required'  => array('wxeo-shortcode-feature-centered-list-on-off', "=", true),
                    ),

                    array(
                        'id'            => 'wxeo-shortcode-feature-centered-list-subtitle-font',
                        'type'          => 'typography',
                        'title'         => __('Subtitle Typography', 'wxeo-wun'),
                        'google'        => true,
                        'font-backup'   => true,
                        'word-spacing'  => true,
                        'letter-spacing'=> true,
                        'all_styles'    => true,
                        'units'         => 'px',
                        'subtitle'      => __('Typography options for the subtitle.', 'wxeo-wun'),
                        'default'       => array(
                            'color'         => '#333',
                            'font-style'    => '700',
                            'font-family'   => 'Abel',
                            'google'        => true,
                            'font-size'     => '33px',
                            'line-height'   => '40px'
                        ),
                        'required'  => array('wxeo-shortcode-feature-centered-list-on-off', "=", true),
                    ),

                    

                ),
                    
            );




      

            $this->sections[] = array(
                'title'     => __('Feature List Boxed', 'wxeo-wun'),
                'subsection' => true,
                'fields'    => array(

                    array(
                        'id'        => 'wxeo-shortcode-feature-box-list-on-off',
                        'type'      => 'switch',
                        'title'     => __('Feature List Status', 'wxeo-wun'),
                        'subtitle'  => __('If turned off the css for featured list wont be compiled saving space.', 'wxeo-wun'),
                        'default'   => true,
                        'on'        => 'Enable',
                        'off'       => 'Disable',
                        'css_house' => true,
                    ),


                    array(
                        'id'        => 'wxeo-shortcode-feature-box-list-icon-background',
                        'type'      => 'image_select',
                        'title'     => __('Icon Style', 'wxeo-wun'),
                        'subtitle'  => __('Milestone icon style type.', 'wxeo-wun'),
                        'options'   => array(
                            'na'    => array('alt' => 'No Background',      'img' => 'http://placehold.it/160x50/cccccc/919191.jpg&text=NO%20BACKGROUND'),
                            'color' => array('alt' => 'Color Background',   'img' => 'http://placehold.it/160x50/cccccc/919191.jpg&text=BACKGROUND'),
                        ),
                        'default'   => 'na',
                        'css_house' => true,
                        // 'js_house'  => true,
                        'required'  => array('wxeo-shortcode-feature-box-list-on-off', "=", true),
                    ),

                    array(
                        'id'        => 'wxeo-shortcode-feature-box-list-icon-color',
                        'type'      => 'color',
                        'title'     => __('Icon Color', 'wxeo-wun'),
                        'subtitle'  => __('Milestone icon color.', 'wxeo-wun'),
                        'default'   => '#909090',
                        'validate'  => 'color',
                        'transparent', false,
                        'css_house' => true,
                        'required'  => array('wxeo-shortcode-feature-box-list-on-off', "=", true),
                    ),

                    array(
                        'id'        => 'wxeo-shortcode-feature-box-list-background-color',
                        'type'      => 'color',
                        'title'     => __('Icon Background Color', 'wxeo-wun'),
                        'subtitle'  => __('Milestone icon background color.', 'wxeo-wun'),
                        'default'   => '#F1F1F1',
                        'validate'  => 'color',
                        'transparent', false,
                        'css_house' => true,
                        'required'  => array(
                            array('wxeo-shortcode-feature-box-list-icon-background', "=", 'color'),
                            array('wxeo-shortcode-feature-box-list-on-off', "=", true),
                        ),
                    ),


                    array(
                        'id'        => 'wxeo-shortcode-feature-box-list-icon-border',
                        'type'      => 'border',
                        'title'     => __('Icon Border', 'wxeo-wun'),
                        'all'       => false,
                        'default'   => array(
                            'border-color'  => '#E5E5E5', 
                            'border-style'  => 'solid', 
                            'border-top'    => '1px',
                            'border-right'  => '1px',
                            'border-bottom' => '1px',
                            'border-left'   => '1px',
                        ),
                        'css_house' => true,
                        'required'  => array(
                            array('wxeo-shortcode-feature-box-list-icon-background', "=", 'color'),
                            array('wxeo-shortcode-feature-box-list-on-off', "=", true),
                        ),
                    ),

                    array(
                        'id'        => 'wxeo-shortcode-feature-box-list-icon-radius',
                        'type'      => 'image_select',
                        'title'     => __('Featured Style', 'wxeo-wun'),
                        'options'   => array(
                            '0px' => array('alt' => 'Squared',  'img' => 'http://placehold.it/160x50/cccccc/919191.jpg&text=0PX'),
                            '5px' => array('alt' => 'Small Radius (5px)', 'img' => 'http://placehold.it/160x50/cccccc/919191.jpg&text=5PX'),
                            '10px' => array('alt' => 'Medium Radius (10px)','img' => 'http://placehold.it/160x50/cccccc/919191.jpg&text=10PX'),
                            '500px' => array('alt' => 'Large Radius (500px)','img' => 'http://placehold.it/160x50/cccccc/919191.jpg&text=500PX'),
                        ),
                        'default'   => '0px',
                        'css_house' => true,
                        'required'  => array('wxeo-shortcode-feature-box-list-icon-background', "=", 'color'),
                    ),


                    array(
                        'id'            => 'wxeo-shortcode-feature-box-list-counter-font',
                        'type'          => 'typography',
                        'title'         => __('Counter Typography', 'wxeo-wun'),
                        'google'        => true,
                        'font-backup'   => true,
                        'word-spacing'  => true,
                        'letter-spacing'=> true,
                        'all_styles'    => true,
                        'units'         => 'px',
                        'subtitle'      => __('Typography options for the counter.', 'wxeo-wun'),
                        // 'compiler' => true,
                        'default'       => array(
                            'color'         => '#333',
                            'font-style'    => '700',
                            'font-family'   => 'Abel',
                            'google'        => true,
                            'font-size'     => '33px',
                            'line-height'   => '40px'
                        ),
                        'required'  => array('wxeo-shortcode-feature-box-list-on-off', "=", true),
                    ),

                    array(
                        'id'            => 'wxeo-shortcode-feature-box-list-subtitle-font',
                        'type'          => 'typography',
                        'title'         => __('Subtitle Typography', 'wxeo-wun'),
                        'google'        => true,
                        'font-backup'   => true,
                        'word-spacing'  => true,
                        'letter-spacing'=> true,
                        'all_styles'    => true,
                        'units'         => 'px',
                        'subtitle'      => __('Typography options for the subtitle.', 'wxeo-wun'),
                        'default'       => array(
                            'color'         => '#333',
                            'font-style'    => '700',
                            'font-family'   => 'Abel',
                            'google'        => true,
                            'font-size'     => '33px',
                            'line-height'   => '40px'
                        ),
                        'required'  => array('wxeo-shortcode-feature-box-list-on-off', "=", true),
                    ),

                    

                ),
                    
            );




            $this->sections[] = array(
                'title'     => __('Tabs', 'wxeo-wun'),
                'subsection' => true,
                'fields'    => array(



                    array(
                        'id'        => 'wxeo-shortcode-tabs-style',
                        'type'      => 'image_select',
                        'title'     => __('Icon Style', 'wxeo-wun'),
                        'options'   => array(
                            'st'    => array('alt' => 'Snug Tab',       'img' => 'http://placehold.it/160x50/cccccc/919191.jpg&text=SNUG%20TAB'),
                            'ib'    => array('alt' => 'Indie Button',   'img' => 'http://placehold.it/160x50/cccccc/919191.jpg&text=INDIE%20BUTTON'),
                            'ut'    => array('alt' => 'Unified Tab',    'img' => 'http://placehold.it/160x50/cccccc/919191.jpg&text=UNIFIED%20TAB'),
                        ),
                        'default'   => 'ut',
                        'css_house' => true,
                    ),


                    array(
                        'id'        => 'wxeo-shortcode-tabs-radius',
                        'type'      => 'image_select',
                        'title'     => __('Tab Radius', 'wxeo-wun'),
                        'options'   => array(
                            '0px' => array('alt' => 'Squared',  'img' => 'http://placehold.it/160x50/cccccc/919191.jpg&text=0PX'),
                            '5px' => array('alt' => 'Small Radius (5px)', 'img' => 'http://placehold.it/160x50/cccccc/919191.jpg&text=5PX'),
                            '10px' => array('alt' => 'Medium Radius (10px)','img' => 'http://placehold.it/160x50/cccccc/919191.jpg&text=10PX'),
                            '500px' => array('alt' => 'Large Radius (500px)','img' => 'http://placehold.it/160x50/cccccc/919191.jpg&text=500PX'),
                        ),
                        'default'   => '0px',
                        'css_house' => true,
                    ),


                    array(
                        'id'        => 'wxeo-shortcode-tabs-content-radius',
                        'type'      => 'image_select',
                        'title'     => __('Tabs Content Radius', 'wxeo-wun'),
                        'options'   => array(
                            '0px' => array('alt' => 'Squared',  'img' => 'http://placehold.it/160x50/cccccc/919191.jpg&text=0PX'),
                            '5px' => array('alt' => 'Small Radius (5px)', 'img' => 'http://placehold.it/160x50/cccccc/919191.jpg&text=5PX'),
                            '10px' => array('alt' => 'Medium Radius (10px)','img' => 'http://placehold.it/160x50/cccccc/919191.jpg&text=10PX'),
                            '500px' => array('alt' => 'Large Radius (500px)','img' => 'http://placehold.it/160x50/cccccc/919191.jpg&text=500PX'),
                        ),
                        'default'   => '0px',
                        'css_house' => true,
                    ),



                    array(
                        'id'        => 'wxeo-shortcode-tabs-border',
                        'type'      => 'border',
                        'title'     => __('Tabs Border', 'wxeo-wun'),
                        'all'       => false,
                        'default'   => array(
                            'border-color'  => '#E5E5E5', 
                            'border-style'  => 'solid', 
                            'border-top'    => '1px',
                            'border-right'  => '1px',
                            'border-bottom' => '1px',
                            'border-left'   => '1px',
                        ),
                        'css_house' => true,
                    ),

                    
                    array(
                        'id'        => 'wxeo-shortcode-tabs-background-color',
                        'type'      => 'color',
                        'title'     => __('Tabs Content Background Color', 'wxeo-wun'),
                        'default'   => '#ffffff',
                        'validate'  => 'color',
                        'transparent', false,
                        'css_house' => true,
                    ),


                    array(
                        'id'        => 'wxeo-shortcode-tabs-active-background-color',
                        'type'      => 'color',
                        'title'     => __('Active Tab Background Color', 'wxeo-wun'),
                        'default'   => '#ffffff',
                        'validate'  => 'color',
                        'transparent', false,
                        'css_house' => true,
                        'required'  => array('wxeo-shortcode-tabs-style', "!=", 'st'),
                    ),

                    array(
                        'id'        => 'wxeo-shortcode-tabs-active-background-color-hover',
                        'type'      => 'color',
                        'title'     => __('Active Tab Hover Background Color', 'wxeo-wun'),
                        'default'   => '#ffffff',
                        'validate'  => 'color',
                        'transparent', false,
                        'css_house' => true,
                        'required'  => array('wxeo-shortcode-tabs-style', "!=", 'st'),
                    ),


                    array(
                        'id'        => 'wxeo-shortcode-tabs-active-border',
                        'type'      => 'border',
                        'title'     => __('Active Tab Border', 'wxeo-wun'),
                        'all'       => false,
                        'default'   => array(
                            'border-color'  => '#E5E5E5', 
                            'border-style'  => 'solid', 
                            'border-top'    => '1px',
                            'border-right'  => '1px',
                            'border-bottom' => '1px',
                            'border-left'   => '1px',
                        ),
                        'css_house' => true,
                        'required'  => array('wxeo-shortcode-tabs-style', "!=", 'st'),
                    ),

                    array(
                        'id'   =>'divider_1',
                        'type' => 'divide'
                    ),


                    array(
                        'id'        => 'wxeo-shortcode-tabs-inactive-background-color',
                        'type'      => 'color',
                        'title'     => __('Inactive Tab Background Color', 'wxeo-wun'),
                        'default'   => '#ffffff',
                        'validate'  => 'color',
                        'transparent', false,
                        'css_house' => true,
                    ),

                    array(
                        'id'        => 'wxeo-shortcode-tabs-inactive-background-color-hover',
                        'type'      => 'color',
                        'title'     => __('Inactive Tab Hover Background Color', 'wxeo-wun'),
                        'default'   => '#ffffff',
                        'validate'  => 'color',
                        'transparent', false,
                        'css_house' => true,
                    ),


                    array(
                        'id'        => 'wxeo-shortcode-tabs-inactive-border',
                        'type'      => 'border',
                        'title'     => __('Inactive Tab Border', 'wxeo-wun'),
                        'all'       => false,
                        'default'   => array(
                            'border-color'  => '#E5E5E5', 
                            'border-style'  => 'solid', 
                            'border-top'    => '1px',
                            'border-right'  => '1px',
                            'border-bottom' => '1px',
                            'border-left'   => '1px',
                        ),
                        'css_house' => true,
                    ),





                ),
                    
            );




            $this->sections[] = array(
                'title'     => __('Tour', 'wxeo-wun'),
                'subsection' => true,
                'fields'    => array(



                    array(
                        'id'        => 'wxeo-shortcode-tour-style',
                        'type'      => 'image_select',
                        'title'     => __('Icon Style', 'wxeo-wun'),
                        'options'   => array(
                            'st'    => array('alt' => 'Snug Tab',       'img' => 'http://placehold.it/160x50/cccccc/919191.jpg&text=SNUG%20TAB'),
                            'ib'    => array('alt' => 'Indie Button',   'img' => 'http://placehold.it/160x50/cccccc/919191.jpg&text=INDIE%20BUTTON'),
                            'ut'    => array('alt' => 'Unified Tab',    'img' => 'http://placehold.it/160x50/cccccc/919191.jpg&text=UNIFIED%20TAB'),
                        ),
                        'default'   => 'ut',
                        'css_house' => true,
                    ),


                    array(
                        'id'        => 'wxeo-shortcode-tour-radius',
                        'type'      => 'image_select',
                        'title'     => __('Tab Radius', 'wxeo-wun'),
                        'options'   => array(
                            '0px' => array('alt' => 'Squared',  'img' => 'http://placehold.it/160x50/cccccc/919191.jpg&text=0PX'),
                            '5px' => array('alt' => 'Small Radius (5px)', 'img' => 'http://placehold.it/160x50/cccccc/919191.jpg&text=5PX'),
                            '10px' => array('alt' => 'Medium Radius (10px)','img' => 'http://placehold.it/160x50/cccccc/919191.jpg&text=10PX'),
                            '500px' => array('alt' => 'Large Radius (500px)','img' => 'http://placehold.it/160x50/cccccc/919191.jpg&text=500PX'),
                        ),
                        'default'   => '0px',
                        'css_house' => true,
                    ),


                    array(
                        'id'        => 'wxeo-shortcode-tour-content-radius',
                        'type'      => 'image_select',
                        'title'     => __('Tabs Content Radius', 'wxeo-wun'),
                        'options'   => array(
                            '0px' => array('alt' => 'Squared',  'img' => 'http://placehold.it/160x50/cccccc/919191.jpg&text=0PX'),
                            '5px' => array('alt' => 'Small Radius (5px)', 'img' => 'http://placehold.it/160x50/cccccc/919191.jpg&text=5PX'),
                            '10px' => array('alt' => 'Medium Radius (10px)','img' => 'http://placehold.it/160x50/cccccc/919191.jpg&text=10PX'),
                            '500px' => array('alt' => 'Large Radius (500px)','img' => 'http://placehold.it/160x50/cccccc/919191.jpg&text=500PX'),
                        ),
                        'default'   => '0px',
                        'css_house' => true,
                    ),



                    array(
                        'id'        => 'wxeo-shortcode-tour-border',
                        'type'      => 'border',
                        'title'     => __('Tabs Border', 'wxeo-wun'),
                        'all'       => false,
                        'default'   => array(
                            'border-color'  => '#E5E5E5', 
                            'border-style'  => 'solid', 
                            'border-top'    => '1px',
                            'border-right'  => '1px',
                            'border-bottom' => '1px',
                            'border-left'   => '1px',
                        ),
                        'css_house' => true,
                    ),

                    
                    array(
                        'id'        => 'wxeo-shortcode-tour-background-color',
                        'type'      => 'color',
                        'title'     => __('Tabs Content Background Color', 'wxeo-wun'),
                        'default'   => '#ffffff',
                        'validate'  => 'color',
                        'transparent', false,
                        'css_house' => true,
                    ),


                    array(
                        'id'        => 'wxeo-shortcode-tour-active-background-color',
                        'type'      => 'color',
                        'title'     => __('Active Tab Background Color', 'wxeo-wun'),
                        'default'   => '#ffffff',
                        'validate'  => 'color',
                        'transparent', false,
                        'css_house' => true,
                        'required'  => array('wxeo-shortcode-tour-style', "!=", 'st'),
                    ),

                    array(
                        'id'        => 'wxeo-shortcode-tour-active-background-color-hover',
                        'type'      => 'color',
                        'title'     => __('Active Tab Hover Background Color', 'wxeo-wun'),
                        'default'   => '#ffffff',
                        'validate'  => 'color',
                        'transparent', false,
                        'css_house' => true,
                        'required'  => array('wxeo-shortcode-tour-style', "!=", 'st'),
                    ),


                    array(
                        'id'        => 'wxeo-shortcode-tour-active-border',
                        'type'      => 'border',
                        'title'     => __('Active Tab Border', 'wxeo-wun'),
                        'all'       => false,
                        'default'   => array(
                            'border-color'  => '#E5E5E5', 
                            'border-style'  => 'solid', 
                            'border-top'    => '1px',
                            'border-right'  => '1px',
                            'border-bottom' => '1px',
                            'border-left'   => '1px',
                        ),
                        'css_house' => true,
                        'required'  => array('wxeo-shortcode-tour-style', "!=", 'st'),
                    ),

                    array(
                        'id'   =>'divider_1',
                        'type' => 'divide'
                    ),


                    array(
                        'id'        => 'wxeo-shortcode-tour-inactive-background-color',
                        'type'      => 'color',
                        'title'     => __('Inactive Tab Background Color', 'wxeo-wun'),
                        'default'   => '#ffffff',
                        'validate'  => 'color',
                        'transparent', false,
                        'css_house' => true,
                    ),

                    array(
                        'id'        => 'wxeo-shortcode-tour-inactive-background-color-hover',
                        'type'      => 'color',
                        'title'     => __('Inactive Tab Hover Background Color', 'wxeo-wun'),
                        'default'   => '#ffffff',
                        'validate'  => 'color',
                        'transparent', false,
                        'css_house' => true,
                    ),


                    array(
                        'id'        => 'wxeo-shortcode-tour-inactive-border',
                        'type'      => 'border',
                        'title'     => __('Inactive Tab Border', 'wxeo-wun'),
                        'all'       => false,
                        'default'   => array(
                            'border-color'  => '#E5E5E5', 
                            'border-style'  => 'solid', 
                            'border-top'    => '1px',
                            'border-right'  => '1px',
                            'border-bottom' => '1px',
                            'border-left'   => '1px',
                        ),
                        'css_house' => true,
                    ),





                ),
                    
            );





            $this->sections[] = array(
                'title'     => __('Toggle Section', 'wxeo-wun'),
                'subsection' => true,
                'fields'    => array(



                    array(
                        'id'        => 'wxeo-shortcode-toggle-style',
                        'type'      => 'image_select',
                        'title'     => __('Icon Style', 'wxeo-wun'),
                        'options'   => array(
                            'snug'    => array('alt' => 'Snug',       'img' => 'http://placehold.it/160x50/cccccc/919191.jpg&text=SNUG'),
                            'spaced'    => array('alt' => 'Spaced',   'img' => 'http://placehold.it/160x50/cccccc/919191.jpg&text=SPACED'),
                        ),
                        'default'   => 'snug',
                        'css_house' => true,
                    ),


                    array(
                        'id'        => 'wxeo-shortcode-toggle-radius',
                        'type'      => 'image_select',
                        'title'     => __('Tab Radius', 'wxeo-wun'),
                        'options'   => array(
                            '0px' => array('alt' => 'Squared',  'img' => 'http://placehold.it/160x50/cccccc/919191.jpg&text=0PX'),
                            '5px' => array('alt' => 'Small Radius (5px)', 'img' => 'http://placehold.it/160x50/cccccc/919191.jpg&text=5PX'),
                            '10px' => array('alt' => 'Medium Radius (10px)','img' => 'http://placehold.it/160x50/cccccc/919191.jpg&text=10PX'),
                            '500px' => array('alt' => 'Large Radius (500px)','img' => 'http://placehold.it/160x50/cccccc/919191.jpg&text=500PX'),
                        ),
                        'default'   => '0px',
                        'css_house' => true,
                    ),


                    array(
                        'id'        => 'wxeo-shortcode-toggle-content-radius',
                        'type'      => 'image_select',
                        'title'     => __('Tabs Content Radius', 'wxeo-wun'),
                        'options'   => array(
                            '0px' => array('alt' => 'Squared',  'img' => 'http://placehold.it/160x50/cccccc/919191.jpg&text=0PX'),
                            '5px' => array('alt' => 'Small Radius (5px)', 'img' => 'http://placehold.it/160x50/cccccc/919191.jpg&text=5PX'),
                            '10px' => array('alt' => 'Medium Radius (10px)','img' => 'http://placehold.it/160x50/cccccc/919191.jpg&text=10PX'),
                            '500px' => array('alt' => 'Large Radius (500px)','img' => 'http://placehold.it/160x50/cccccc/919191.jpg&text=500PX'),
                        ),
                        'default'   => '0px',
                        'css_house' => true,
                    ),


                    array(
                        'id'        => 'wxeo-shortcode-toggle-border',
                        'type'      => 'border',
                        'title'     => __('Tabs Border', 'wxeo-wun'),
                        'default'   => array(
                            'border-color'  => '#E6E6E6', 
                            'border-style'  => 'solid', 
                            'border-width'  => '1px',
                        ),
                        'css_house' => true,
                    ),


                    array(
                        'id'        => 'wxeo-shortcode-toggle-content-border',
                        'type'      => 'border',
                        'title'     => __('Tabs Border', 'wxeo-wun'),
                        'default'   => array(
                            'border-color'  => '#E6E6E6', 
                            'border-style'  => 'solid', 
                            'border-width'  => '1px',
                        ),
                        'css_house' => true,
                    ),

                    
                    array(
                        'id'        => 'wxeo-shortcode-toggle-background-color',
                        'type'      => 'color',
                        'title'     => __('Toggle Tab Background', 'wxeo-wun'),
                        'default'   => '#ffffff',
                        'validate'  => 'color',
                        'transparent', false,
                        'css_house' => true,
                    ),


                    array(
                        'id'        => 'wxeo-shortcode-toggle-active-background-color',
                        'type'      => 'color',
                        'title'     => __('Toggle Tab Open Background', 'wxeo-wun'),
                        'default'   => '#ffffff',
                        'validate'  => 'color',
                        'transparent', false,
                        'css_house' => true,
                        'required'  => array('wxeo-shortcode-toggle-style', "!=", 'st'),
                    ),



                    array(
                        'id'        => 'wxeo-shortcode-toggle-content-background-color',
                        'type'      => 'color',
                        'title'     => __('Toggle Content Background', 'wxeo-wun'),
                        'default'   => '#ffffff',
                        'validate'  => 'color',
                        'transparent', false,
                        'css_house' => true,
                        'required'  => array('wxeo-shortcode-toggle-style', "!=", 'st'),
                    ),



                ),
                    
            );






            $this->sections[] = array(
                'title'     => __('Accordion Section', 'wxeo-wun'),
                'subsection' => true,
                'fields'    => array(



                    array(
                        'id'        => 'wxeo-shortcode-accordion-style',
                        'type'      => 'image_select',
                        'title'     => __('Icon Style', 'wxeo-wun'),
                        'options'   => array(
                            'snug'    => array('alt' => 'Snug',       'img' => 'http://placehold.it/160x50/cccccc/919191.jpg&text=SNUG'),
                            'spaced'    => array('alt' => 'Spaced',   'img' => 'http://placehold.it/160x50/cccccc/919191.jpg&text=SPACED'),
                        ),
                        'default'   => 'snug',
                        'css_house' => true,
                    ),


                    array(
                        'id'        => 'wxeo-shortcode-accordion-radius',
                        'type'      => 'image_select',
                        'title'     => __('Tab Radius', 'wxeo-wun'),
                        'options'   => array(
                            '0px' => array('alt' => 'Squared',  'img' => 'http://placehold.it/160x50/cccccc/919191.jpg&text=0PX'),
                            '5px' => array('alt' => 'Small Radius (5px)', 'img' => 'http://placehold.it/160x50/cccccc/919191.jpg&text=5PX'),
                            '10px' => array('alt' => 'Medium Radius (10px)','img' => 'http://placehold.it/160x50/cccccc/919191.jpg&text=10PX'),
                            '500px' => array('alt' => 'Large Radius (500px)','img' => 'http://placehold.it/160x50/cccccc/919191.jpg&text=500PX'),
                        ),
                        'default'   => '0px',
                        'css_house' => true,
                    ),


                    array(
                        'id'        => 'wxeo-shortcode-accordion-content-radius',
                        'type'      => 'image_select',
                        'title'     => __('Tabs Content Radius', 'wxeo-wun'),
                        'options'   => array(
                            '0px' => array('alt' => 'Squared',  'img' => 'http://placehold.it/160x50/cccccc/919191.jpg&text=0PX'),
                            '5px' => array('alt' => 'Small Radius (5px)', 'img' => 'http://placehold.it/160x50/cccccc/919191.jpg&text=5PX'),
                            '10px' => array('alt' => 'Medium Radius (10px)','img' => 'http://placehold.it/160x50/cccccc/919191.jpg&text=10PX'),
                            '500px' => array('alt' => 'Large Radius (500px)','img' => 'http://placehold.it/160x50/cccccc/919191.jpg&text=500PX'),
                        ),
                        'default'   => '0px',
                        'css_house' => true,
                    ),


                    array(
                        'id'        => 'wxeo-shortcode-accordion-border',
                        'type'      => 'border',
                        'title'     => __('Tabs Border', 'wxeo-wun'),
                        'default'   => array(
                            'border-color'  => '#E6E6E6', 
                            'border-style'  => 'solid', 
                            'border-width'  => '1px',
                        ),
                        'css_house' => true,
                    ),


                    array(
                        'id'        => 'wxeo-shortcode-accordion-content-border',
                        'type'      => 'border',
                        'title'     => __('Tabs Border', 'wxeo-wun'),
                        'default'   => array(
                            'border-color'  => '#E6E6E6', 
                            'border-style'  => 'solid', 
                            'border-width'  => '1px',
                        ),
                        'css_house' => true,
                    ),

                    
                    array(
                        'id'        => 'wxeo-shortcode-accordion-background-color',
                        'type'      => 'color',
                        'title'     => __('Toggle Tab Background', 'wxeo-wun'),
                        'default'   => '#ffffff',
                        'validate'  => 'color',
                        'transparent', false,
                        'css_house' => true,
                    ),


                    array(
                        'id'        => 'wxeo-shortcode-accordion-active-background-color',
                        'type'      => 'color',
                        'title'     => __('Toggle Tab Open Background', 'wxeo-wun'),
                        'default'   => '#ffffff',
                        'validate'  => 'color',
                        'transparent', false,
                        'css_house' => true,
                        'required'  => array('wxeo-shortcode-accordion-style', "!=", 'st'),
                    ),



                    array(
                        'id'        => 'wxeo-shortcode-accordion-content-background-color',
                        'type'      => 'color',
                        'title'     => __('Toggle Content Background', 'wxeo-wun'),
                        'default'   => '#ffffff',
                        'validate'  => 'color',
                        'transparent', false,
                        'css_house' => true,
                        'required'  => array('wxeo-shortcode-accordion-style', "!=", 'st'),
                    ),



                ),
                    
            );
			
		
            
            $this->sections[] = array(
                'title'     => __('Blog', 'wxeo-wun'),
                'fields'    => array(

                    array(
                        'id'        => 'wxeo-blog-style',
                        'type'      => 'image_select',
                        'title'     => __('Blog Style', 'wxeo-wun'),
                        'options'   => array(
                                'thumbnail'     => array('alt' => 'Thumbnail Posts',    'img' => 'http://placehold.it/160x50/cccccc/919191.jpg&text=THUMBNAIL+POSTS'),
                                'grid'          => array('alt' => 'Grid Posts',         'img' => 'http://placehold.it/160x50/cccccc/919191.jpg&text=GRID+POSTS'),
                                'isotope'       => array('alt' => 'Isotope Posts',      'img' => 'http://placehold.it/160x50/cccccc/919191.jpg&text=ISOTOPE+POSTS'),
                                'wide'          => array('alt' => 'Wide Posts',         'img' => 'http://placehold.it/160x50/cccccc/919191.jpg&text=WIDE+POSTS'),
                                'wide-center'   => array('alt' => 'Wide Center Posts',  'img' => 'http://placehold.it/160x50/cccccc/919191.jpg&text=WIDE+CENTER+POSTS')
                            ),
                        'default'   => 'wide',
                        // 'css_house' => true,
                        // 'js_house'  => true,
                    ),

                    
                    array(
                        'id'        => 'wxeo-blog-sidebar',
                        'type'      => 'switch',
                        'title'     => __('Blog Sidebar', 'wxeo-wun'),
                        'default'   => true,
                    ),

                    array(
                        'id'        => 'wxeo-blog-image-height',
                        'type'      => 'image_select',
                        'title'     => __('Blog Style', 'wxeo-wun'),
                        'options'   => array(
                                'small'     => array('alt' => 'Small',      'img' => 'http://placehold.it/160x50/cccccc/919191.jpg&text=SMALL'),
                                'medium'    => array('alt' => 'Medium',     'img' => 'http://placehold.it/160x50/cccccc/919191.jpg&text=MEDIUM'),
                                'large'     => array('alt' => 'Large',      'img' => 'http://placehold.it/160x50/cccccc/919191.jpg&text=LARGE'),
                                'any'       => array('alt' => 'Any Size',   'img' => 'http://placehold.it/160x50/cccccc/919191.jpg&text=ANY+SIZE'),
                            ),
                        'default'   => 'medium',
                        // 'css_house' => true,
                        // 'js_house'  => true,
                    ),



                    array(
                        'id'        => 'wxeo-blog-single-sidebar',
                        'type'      => 'switch',
                        'title'     => __('Blog Sidebar Single Post', 'wxeo-wun'),
                        'default'   => true,
                    ),

                    array(
                        'id'        => 'wxeo-blog-single-image-height',
                        'type'      => 'image_select',
                        'title'     => __('Blog Style', 'wxeo-wun'),
                        'options'   => array(
                                'small'     => array('alt' => 'Small',      'img' => 'http://placehold.it/160x50/cccccc/919191.jpg&text=SMALL'),
                                'medium'    => array('alt' => 'Medium',     'img' => 'http://placehold.it/160x50/cccccc/919191.jpg&text=MEDIUM'),
                                'large'     => array('alt' => 'Large',      'img' => 'http://placehold.it/160x50/cccccc/919191.jpg&text=LARGE'),
                                'any'       => array('alt' => 'Any Size',   'img' => 'http://placehold.it/160x50/cccccc/919191.jpg&text=ANY+SIZE'),
                            ),
                        'default'   => 'medium',
                        // 'css_house' => true,
                        // 'js_house'  => true,
                    ),

                    array(
                        'id'        => 'wxeo-blog-single-timestamp',
                        'type'      => 'image_select',
                        'title'     => __('Blog Style', 'wxeo-wun'),
                        'options'   => array(
                                'content-left'  => array('alt' => 'Content Left',   'img' => 'http://placehold.it/160x50/cccccc/919191.jpg&text=CONTENT+LEFT'),
                                'over-image'    => array('alt' => 'Over Image',     'img' => 'http://placehold.it/160x50/cccccc/919191.jpg&text=OVER+IMAGE'),
                                'top-left'      => array('alt' => 'Top left',       'img' => 'http://placehold.it/160x50/cccccc/919191.jpg&text=TOP LEFT'),
                                'meta'          => array('alt' => 'Meta Section',   'img' => 'http://placehold.it/160x50/cccccc/919191.jpg&text=META+SECTION'),
                            ),
                        'default'   => 'content-left',
                        // 'css_house' => true,
                        // 'js_house'  => true,
                    ),

                    array(
                        'id'        => 'wxeo-blog-single-timestamp-style',
                        'type'      => 'image_select',
                        'title'     => __('Blog Style', 'wxeo-wun'),
                        'options'   => array(
                                'blog-timestamp-fill'  => array('alt' => 'Filled',   'img' => 'http://placehold.it/160x50/cccccc/919191.jpg&text=FILL'),
                                'blog-timestamp-border'=> array('alt' => 'Border',     'img' => 'http://placehold.it/160x50/cccccc/919191.jpg&text=BORDER'),
                            ),
                        'default'   => 'blog-timestamp-fill',
                        // 'css_house' => true,
                        // 'js_house'  => true,
                    ),
                    
                ),
                    
            );


            
                  

          
        }

        /**

          All the possible arguments for Redux.
          For full documentation on arguments, please refer to: https://github.com/ReduxFramework/ReduxFramework/wiki/Arguments

         * */
        public function setArguments() {

            $theme = wp_get_theme(); // For use with some settings. Not necessary.

            $this->args = array(
                // TYPICAL -> Change these values as you need/desire
                'opt_name'          => 'wxeo_wun',            // This is where your data is stored in the database and also becomes your global variable name.
                'display_name'      => $theme->get('Name'),     // Name that appears at the top of your panel
                'display_version'   => $theme->get('Version'),  // Version that appears at the top of your panel
                'menu_type'         => 'menu',                  //Specify if the admin menu should appear or not. Options: menu or submenu (Under appearance only)
                'allow_sub_menu'    => true,                    // Show the sections below the admin menu item or not
                'menu_title'        => __('Wun', 'wxeo-wun'),
                'page_title'        => __('Wun', 'wxeo-wun'),
                'google_api_key'    => 'AIzaSyDnqL6zi0RqB1BM36F17-0KkQg565uEBgM',
                // 'async_typography'  => true,


                'admin_bar'         => true,                    // Show the panel pages on the admin bar
                'dev_mode'          => true,                    // Show the time the page took to load, etc
                
                
            );


        }

    }
    
    global $reduxConfig;
    $reduxConfig = new WXEO_Wun_Options();
}
