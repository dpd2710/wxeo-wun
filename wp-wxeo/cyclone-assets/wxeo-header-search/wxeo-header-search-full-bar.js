side_line_search_width = (function () {

	$('.navigation-side-line .search-slide').width( ($(window).width() - $('.navigation-side-line').width() ) );

	$('.wxeo-search a').on('click', function() {
		$('.search-slide').fadeIn();
		$('#search-slide-input').focus();
	});

	$('#search-slide-close').on('click', function() {
		$('.search-slide').fadeOut();
	});


})(),