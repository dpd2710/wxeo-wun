<?php
/**
 * Custom Search Widget
 ******************************************/

function search_widget( $form ) {
    $form = '<form role="search" method="get" id="searchform" class="searchform" action="' . home_url( '/' ) . '" >
              <div class="input-group">
                <input type="text" class="form-control" value="' . get_search_query() . '" name="s">
                <span class="input-group-btn">
                  <button class="btn btn-default" type="submit">'. esc_attr__( 'Search' ) .'</button>
                </span>
              </div><!-- /input-group -->
            </form>';

    return $form;
}

add_filter( 'get_search_form', 'search_widget' );