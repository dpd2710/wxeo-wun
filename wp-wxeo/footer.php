<?php
/**
 * Footer Funtions
 ******************************************/

// Footer Widgets
if(wxeo_wun('wxeo-footer-style') == 'one-column') { 
if (function_exists('register_sidebar')) {
    register_sidebar(array(
        'name'=> 'One Column Footer',
        'id' => 'one-column-footer',
        'before_widget' => '<div id="%1$s" class="widget col-md-1 %2$s">',
        'after_widget' => '</div>',
        'before_title' => '<h2 class="offscreen">',
        'after_title' => '</h2>',
    ));
    }
} 

if(wxeo_wun('wxeo-footer-style') == 'two-column') { 
if (function_exists('register_sidebar')) {
    register_sidebar(array(
        'name'=> 'Two Column Footer',
        'id' => 'two-column-footer',
        'before_widget' => '<div id="%1$s" class="widget col-md-6 %2$s">',
        'after_widget' => '</div>',
        'before_title' => '<h2 class="offscreen">',
        'after_title' => '</h2>',
    ));
    }
} 
if(wxeo_wun('wxeo-footer-style') == 'three-column') { 
if (function_exists('register_sidebar')) {
    register_sidebar(array(
        'name'=> 'Three Column Footer',
        'id' => 'three-column-footer',
        'before_widget' => '<div id="%1$s" class="widget col-md-4 %2$s">',
        'after_widget' => '</div>',
        'before_title' => '<h2 class="offscreen">',
        'after_title' => '</h2>',
    ));
    }
} 
if(wxeo_wun('wxeo-footer-style') == 'four-column') { 
if (function_exists('register_sidebar')) {
    register_sidebar(array(
        'name'=> 'Four Column Footer',
        'id' => 'four-column-footer',
        'before_widget' => '<div id="%1$s" class="widget col-md-3 %2$s">',
        'after_widget' => '</div>',
        'before_title' => '<h2 class="offscreen">',
        'after_title' => '</h2>',
    ));
    }
} 
