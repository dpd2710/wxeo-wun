<?php
/**
 * The main template file.
 *
 * This is the most generic template file in a WordPress theme
 * and one of the two required files for a theme (the other being style.css).
 * It is used to display a page when nothing more specific matches a query.
 * E.g., it puts together the home page when no home.php file exists.
 * Learn more: http://codex.wordpress.org/Template_Hierarchy
 *
 * @package WXEO Wun
 */

get_header(); ?>
	
	<div id="wxeo-page-heading">
		<h1><?php echo $post->post_name; ?></h1>
	</div>
	
	<div class="container <?php if (wxeo_wun('wxeo-blog-sidebar')) { echo 'asidebar'; } ?>">
			<div class="row">
			  <div class="col-md-<?php if (wxeo_wun('wxeo-blog-sidebar')) { echo '9'; } else { echo '12'; } ?>">

					<main id="main" class="site-main" role="main">
						
						<?php if ( have_posts() ) : ?>
			
							<?php 
								while ( have_posts() ) : the_post();
									get_template_part( 'posts');
								endwhile;
							?>

							<?php wxeo_paging_nav(); ?>

						<?php else : ?>

							<?php  get_template_part( 'wp-wxeo/assets/templates/content', 'none' ); ?>

						<?php endif; ?>
					</main>

				</div>
			  <?php if (wxeo_wun('wxeo-blog-sidebar')) : ?>
			  <div class="col-md-3"><?php get_sidebar(); ?></div>
				<?php endif; ?>
			</div>

	</div><!-- .container -->

<?php get_footer(); ?>
