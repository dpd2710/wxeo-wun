<?php get_header(); ?>
	
	<div id="wxeo-page-heading">
		<?php the_title( '<h1>', '</h1>' ); ?>
	</div>
	
	<div class="container">
		<div class="row">
		  <div class="col-md-12">

				<main id="main" class="site-main" role="main">
					<?php woocommerce_content(); ?>
				</main>

			
				<?php if ( comments_open() || '0' != get_comments_number() ) { comments_template(); } // Page Comments ?>

			</div>
		</div>

	</div><!-- .container -->



<?php get_footer(); ?>
