<?php
/**
 * The Header for our theme.
 *
 * Displays all of the <head> section and everything up till <div id="content">
 *
 * @package WXEO Wun
 */
?><!DOCTYPE html>
<html <?php language_attributes(); ?>>
<head>
<meta charset="<?php bloginfo( 'charset' ); ?>">
<meta name="viewport" content="width=device-width, initial-scale=1">
<title><?php wp_title( '|', true, 'right' ); ?></title>
<link rel="profile" href="http://gmpg.org/xfn/11">
<link rel="pingback" href="<?php bloginfo( 'pingback_url' ); ?>">

<link href="//netdna.bootstrapcdn.com/font-awesome/4.1.0/css/font-awesome.css" rel="stylesheet">


<?php wp_head(); ?>


</head>

<body <?php body_class(); ?>>
	<?php if ( wxeo_wun('wxeo-header-style') == 'two-tier' ) : ?>
	<div class="two-tier">
	<div class="container">
		<div class="logo logo-center <?php echo wxeo_wun('wxeo-header-logo-position'); ?>">
	   	<a href="<?php echo esc_url( home_url( '/' ) ); ?>" rel="home">
	     	<img alt="<?php bloginfo( 'name' ); ?>" src="http://placehold.it/300x50">
	    </a>
	  </div>
	</div>  
  </div>
	<?php endif; ?>

 	<!-- menu-center -->
 	<?php 
 		$wxeo_header_size = wxeo_wun('wxeo-header-size');
 		if (wxeo_wun('wxeo-header-style') == 'two-tier') {
 			switch (wxeo_wun('wxeo-header-size')) {
 				case 'sm':
 					$wxeo_header_size = "xs";
 					break;
 				
 				case 'md':
 					$wxeo_header_size = "sm";
 					break;

 				case 'lg':
 					$wxeo_header_size = "md";
 					break;
 			}
 		}

 	?>


  <header id="header" class="navigation<?php if (wxeo_wun('wxeo-header-style') == 'side-line'){echo "-side-line";} ?> 
  nav-<?php //echo $wxeo_header_size." ".wxeo_wun()['wxeo-header-menu-center']; ?>" role="navigation">
    <div class="container">

        <?php if (wxeo_wun('wxeo-header-style') != 'two-tier') : ?>
					<div class="logo logo-center <?php echo wxeo_wun('wxeo-header-logo-position'); ?>">
				   	<a href="<?php echo esc_url( home_url( '/' ) ); ?>" rel="home">
				     	<img alt="<?php bloginfo( 'name' ); ?>" src="http://placehold.it/300x50">
				    </a>
				  </div>
				<?php endif; ?>

        <div class="menu-container visible-xs"> 
        	<div class="responsive"><a href="#">
        		<span class="rod"></span>
        		<span class="rod"></span>
        		<span class="rod"></span>
        	</a></div>
        </div>

        <!-- <div class="divide md"></div> -->

        <nav id="navigation" class="menu-container">

        	


          <ul class="menu mini-menu <?php echo wxeo_wun('wxeo-header-mini-menu-position'); ?>-menu">           
                      	
            <?php

            function icon_list($icon_id, $fa_icon) {
              $full = '';
              if (wxeo_wun('wxeo-social-style') == 'full') {
                $full = 'bttn-full';
              }
              echo '<li class="bttn square '.$full.'"><a href="'.wxeo_wun($icon_id).'"><i class="fa '.$fa_icon.'"></i></a></li>';
            }

            if (wxeo_wun('wxeo-social-facebook-url')) { icon_list('wxeo-social-facebook-url', 'fa-facebook'); }
            if (wxeo_wun('wxeo-social-twitter-url')) { icon_list('wxeo-social-twitter-url', 'fa-twitter'); }
            if (wxeo_wun('wxeo-social-google-url')) { icon_list('wxeo-social-google-url', 'fa-google'); }
            if (wxeo_wun('wxeo-social-linkedin-url')) { icon_list('wxeo-social-linkedin-url', 'fa-linkedin'); }
            if (wxeo_wun('wxeo-social-pinterest-url')) { icon_list('wxeo-social-pinterest-url', 'fa-pinterest'); }
            if (wxeo_wun('wxeo-social-instagram-url')) { icon_list('wxeo-social-instagram-url', 'fa-instagram'); }
            if (wxeo_wun('wxeo-social-flickr-url')) { icon_list('wxeo-social-flickr-url', 'fa-flickr'); }
            if (wxeo_wun('wxeo-social-skype-url')) { icon_list('wxeo-social-skype-url', 'fa-skype'); }
            if (wxeo_wun('wxeo-social-tumblr-url')) { icon_list('wxeo-social-tumblr-url', 'fa-tumblr'); }
            if (wxeo_wun('wxeo-social-youtube-url')) { icon_list('wxeo-social-youtube-url', 'fa-youtube'); }
            if (wxeo_wun('wxeo-social-github-url')) { icon_list('wxeo-social-github-url', 'fa-github'); }
            if (wxeo_wun('wxeo-social-dribbble-url')) { icon_list('wxeo-social-dribbble-url', 'fa-dribbble'); }

            ?>

            <?php if (in_array(wxeo_wun('wxeo-header-search'), array('click-hover', 'full-bar'))) : ?>
            <li class="bttn wxeo-search">
              <a href="#"><i id="wxeo-search-icon" class="fa fa-search"></i></a>
              <?php if (wxeo_wun('wxeo-header-search') == 'click-hover') : ?>
              <div class="search-open">
                <form role="search" method="get" id="searchform" action="<?php echo home_url( '/' ); ?>">
                  <input name="s"  id="search-open-input" type="text" placeholder="Search">
                  <i id="search-open-close" class="fa fa-times"></i>
                </form>
              </div>
              <?php endif; ?>
            </li>
            <?php endif; ?>

            <?php global $woocommerce; ?>
 
            <?php if (class_exists('Woocommerce')) : ?>
            <li class="bttn wxeo-cart"><a  href="<?php echo $woocommerce->cart->get_cart_url(); ?>" title="<?php _e('View your shopping cart', 'woothemes'); ?>"><?php if ($woocommerce->cart->cart_contents_count != '0') { echo '<span>'.$woocommerce->cart->cart_contents_count.'</span>'; } ?><i id="wxeo-cart-icon" class="fa fa-shopping-cart"></i></a></li>
            <?php endif; ?>

          </ul>


          <?php if (wxeo_wun('wxeo-header-search') == 'full-search') : ?>
        	<div class="wxeo-search search-classic">
	        	<div class="row">
						  <div class="col-lg-12">
						    <div class="input-group">
						      <input type="text" class="form-control">
						      <span class="input-group-btn">
						        <button class="btn btn-default" type="button">Search</button>
						      </span>
						    </div><!-- /input-group -->
						  </div><!-- /.col-lg-6 -->
						</div><!-- /.row -->
					</div>
					<?php endif; ?>




          <!-- <div class="divide rt"></div> -->

          	<?php
			
			if ( has_nav_menu( 'primary' ) ) {
							$wxeo_header_btn_size = (wxeo_wun('wxeo-header-style') == 'two-tier') ? wxeo_wun('wxeo-header-btn-size-two-tier') : wxeo_wun('wxeo-header-btn-size');
							wp_nav_menu( array(
								'theme_location' => 'primary',
								'container' =>false,
								'menu_class' => 'menu main-menu '.wxeo_wun('wxeo-header-main-menu-position').'-menu',
								'echo' => true,
								'before' => '',
								'after' => '',
								'link_before' => '',
								'link_after' => '',
								'depth' => 0,
								'walker' => new wxeo_walker(wxeo_wun('wxeo-header-btn-style'), $wxeo_header_btn_size , wxeo_wun('wxeo-header-btn-spacing')))
							);
				} else {
					echo ' please set a primary menu in Appearance > menu ';
					}

      		
						?>
						
					

        </nav>
    </div>

    <?php if (wxeo_wun('wxeo-header-search') == 'full-bar') : ?>
    <div class="wxeo-search">
    	<div class="search-slide">
			<form role="search" method="get" id="searchform" action="<?php echo home_url( '/' ); ?>">
    		<input id="search-slide-input" name="s"  type="text" placeholder="Search">
    		<a id="search-slide-close" href="#"><i class="fa fa-times"></i></a>
			</form>	
    	</div>
    </div>
    <?php endif; ?>

  </header>


<div id="wxeo-container-wrap">