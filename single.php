<?php
/**
 * The Template for displaying all single posts.
 *
 * @package WXEO Wun
 */


get_header(); ?>

	<div id="wxeo-page-heading">
		<h1><?php echo the_title(); ?></h1>
	</div>
	
	<div class="container <?php if (wxeo_wun('wxeo-blog-single-sidebar')) { echo 'asidebar'; } ?>">
			<div class="row">
			  <div class="col-md-<?php if (wxeo_wun('wxeo-blog-single-sidebar')) { echo '9'; } else { echo '12'; } ?>">

					<main id="main" class="site-main" role="main">
						<?php while ( have_posts() ) : the_post(); ?>

							<?php get_template_part( 'wp-wxeo/assets/templates/content', 'single' ); ?>

							<?php wxeo_post_nav(); ?>

							<?php
								// If comments are open or we have at least one comment, load up the comment template
								if ( comments_open() || '0' != get_comments_number() ) :
									comments_template();
								endif;
							?>

						<?php endwhile; // end of the loop. ?>
					</main>

				</div>
				<?php if (wxeo_wun('wxeo-blog-single-sidebar')) : ?>
			  <div class="col-md-3"><?php get_sidebar(); ?></div>
				<?php endif; ?>
			</div>

	</div><!-- .container -->

<?php get_footer(); ?>