jQuery(document).ready(function($) {



var featuresInit = (function () {

		var open = true,

		// ########### HELPERS ###########

    helper1 = function () {
        logMe();
        $('#clickMe').on('click', function () {
            console.log('helper works');
        });
    },

    logMe = function () { console.log('logme ran'); },

    helper2 = function () {
        $('#clickMe').on('click', function () {
            console.log('helper2 works');
        });
    },



    // ########### VARS ###########
    
    circle_bttn = $('#circle_bttn'),

    window_width = $(window).width(),



    // ########### MODULES ###########
    
    blogFeatures = (function () {
        circle_bttn.on('click', function () {
            console.log('this feature has been initialized and works');
        });
    })(),

    portfolio_hover = (function () {

        $( ".portfolio-item" ).hover(
          function() {
            $( this ).children('.wxeo-caption').slideDown();
          }, function() {
            $( this ).children('.wxeo-caption').slideUp();
          }
        );
      
    })();



    return {
        helper1: helper1,
        blogFeatures: blogFeatures,
    };

})();

// console.log(featuresInit.name);
// featuresInit.helper1();
// featuresInit.blogFeatures();



});
