<?php get_header(); ?>
	
	<div id="wxeo-page-heading">
		<?php the_title( '<h1>', '</h1>' ); ?>
	</div>
	
	<div class="container">
		<div class="row">
		  <div class="col-md-12">

				<main id="main" class="site-main" role="main">
					<?php while ( have_posts() ) : the_post(); ?>

						<?php the_content(); ?>
						<?php
							wp_link_pages( array(
								'before' => '<div class="page-links">' . __( 'Pages:', 'wxeo' ),
								'after'  => '</div>',
							) );
						?>


						<article class="blog-classic blog-timestamp-content-left author-pic-article">
							<div class="article-img">
								<img src="http://local.wxeo.com/WXEO/wp-content/uploads/2014/05/tumblr_n6rzwbv3ji1st5lhmo1_1280-1140x380.jpg">
								<div class="author-pic-center"><?php echo get_avatar( get_the_author_meta( 'ID' ), 70 ); ?></div>
							</div>
				      <div class="timestamp pull-left"><time datetime="2014-06-24T22:45:25+00:00" class="updated">20<span>Feb</span><!-- <span class="year">2014</span> --></time></div>
				      <div class="blog-content">
					      <h2>Blog Title Goes Here</h2>
					      <p>This is a sticky post. There are a few things to verify: The sticky post should be distinctly recognizable in some way in comparison to normal posts. You can style the .sticky class if you are using the post_class() function to generate your post classes, which is a best practice. They should…</p>
					      <div class="entry-meta">
					      	<a class="btn btn-border btn-sm read-more">Read More</a>
									<span class="byline"> by <span class="author vcard"><a class="url fn n" href="#">admin</a></span></span>
									<span class="post-meta-devide"> | </span>
									<a href="#" title="View all posts in Uncategorized" rel="category tag">Uncategorized</a>
									<span class="post-meta-devide"> | </span>
									Tag: <a href="#" rel="tag">sticky</a>, <a href="#" rel="tag">template</a>
								</div>
							</div>
							
				    </article>


						<hr>


						<article class="blog-classic blog-timestamp-fill blog-timestamp-block-left author-pic-article">
							<div class="pull-left">
								<div class="timestamp"><time datetime="2014-06-24T22:45:25+00:00" class="updated">20<span>Feb</span><!-- <span class="year">2014</span> --></time></div>
							</div>
							<div class="blog-content">

								<div class="article-img">
									<img src="http://local.wxeo.com/WXEO/wp-content/uploads/2014/05/tumblr_n6rzwbv3ji1st5lhmo1_1280-1140x380.jpg">
									<div class="author-pic tl"><?php echo get_avatar( get_the_author_meta( 'ID' ), 70 ); ?></div>
								</div>
				      
				      
					      <h2>Blog Title Goes Here</h2>
					      <p>This is a sticky post. There are a few things to verify: The sticky post should be distinctly recognizable in some way in comparison to normal posts. You can style the .sticky class if you are using the post_class() function to generate your post classes, which is a best practice. They should…</p>
					      <div class="entry-meta">
					      	<a class="btn btn-border btn-sm read-more">Read More</a>
									<span class="byline"> by <span class="author vcard"><a class="url fn n" href="#">admin</a></span></span>
									<span class="post-meta-devide"> | </span>
									<a href="#" title="View all posts in Uncategorized" rel="category tag">Uncategorized</a>
									<span class="post-meta-devide"> | </span>
									Tag: <a href="#" rel="tag">sticky</a>, <a href="#" rel="tag">template</a>
								</div>
							</div>
							
				    </article>


				    <hr>


						<article class="blog-classic blog-timestamp-fill blog-timestamp-block-left">
							<div class="pull-left">
								<div class="timestamp"><time datetime="2014-06-24T22:45:25+00:00" class="updated">20<span>Feb</span><!-- <span class="year">2014</span> --></time></div>
								<div class="author-pic"><?php echo get_avatar( get_the_author_meta( 'ID' ), 70 ); ?></div>
							</div>
							<div class="blog-content">

								<div class="article-img">
									<img src="http://local.wxeo.com/WXEO/wp-content/uploads/2014/05/tumblr_n6rzwbv3ji1st5lhmo1_1280-1140x380.jpg">
								</div>
				      
				      
					      <h2>Blog Title Goes Here</h2>
					      <p>This is a sticky post. There are a few things to verify: The sticky post should be distinctly recognizable in some way in comparison to normal posts. You can style the .sticky class if you are using the post_class() function to generate your post classes, which is a best practice. They should…</p>
					      <div class="entry-meta">
					      	<a class="btn btn-border btn-sm read-more">Read More</a>
									<span class="byline"> by <span class="author vcard"><a class="url fn n" href="#">admin</a></span></span>
									<span class="post-meta-devide"> | </span>
									<a href="#" title="View all posts in Uncategorized" rel="category tag">Uncategorized</a>
									<span class="post-meta-devide"> | </span>
									Tag: <a href="#" rel="tag">sticky</a>, <a href="#" rel="tag">template</a>
								</div>
							</div>
							
				    </article>


				    <hr>


						<article class="blog-classic blog-timestamp-fill blog-timestamp-block-left">
							<div class="pull-left">
								<div class="timestamp"><time datetime="2014-06-24T22:45:25+00:00" class="updated">20<span>Feb</span><!-- <span class="year">2014</span> --></time></div>
							</div>
							<div class="blog-content">

								<div class="article-img">
									<img src="http://local.wxeo.com/WXEO/wp-content/uploads/2014/05/tumblr_n6rzwbv3ji1st5lhmo1_1280-1140x380.jpg">
								</div>
				      
				      
					      <h2>Blog Title Goes Here</h2>
					      <p>This is a sticky post. There are a few things to verify: The sticky post should be distinctly recognizable in some way in comparison to normal posts. You can style the .sticky class if you are using the post_class() function to generate your post classes, which is a best practice. They should…</p>
					      <div class="entry-meta">
					      	<a class="btn btn-border btn-sm read-more">Read More</a>
									<span class="byline"> by <span class="author vcard"><a class="url fn n" href="#">admin</a></span></span>
									<span class="post-meta-devide"> | </span>
									<a href="#" title="View all posts in Uncategorized" rel="category tag">Uncategorized</a>
									<span class="post-meta-devide"> | </span>
									Tag: <a href="#" rel="tag">sticky</a>, <a href="#" rel="tag">template</a>
								</div>
							</div>
							
				    </article>


				    <hr>


						<article class="blog-classic blog-timestamp-fill blog-timestamp-block-left">
							<div class="pull-left">
								<div class="timestamp pull-left"><time datetime="2014-06-24T22:45:25+00:00" class="updated">20<span>Feb</span><!-- <span class="year">2014</span> --></time></div>
							</div>
							<div class="blog-content">

								<div class="article-img">
									<img src="http://local.wxeo.com/WXEO/wp-content/uploads/2014/05/tumblr_n6rzwbv3ji1st5lhmo1_1280-1140x380.jpg">
								</div>
				      
				      
					      <h2>Blog Title Goes Here</h2>
					      <p>This is a sticky post. There are a few things to verify: The sticky post should be distinctly recognizable in some way in comparison to normal posts. You can style the .sticky class if you are using the post_class() function to generate your post classes, which is a best practice. They should…</p>
					      <div class="entry-meta">
					      	<a class="btn btn-border btn-sm read-more">Read More</a>
									<span class="byline"> by <span class="author vcard"><a class="url fn n" href="#">admin</a></span></span>
									<span class="post-meta-devide"> | </span>
									<a href="#" title="View all posts in Uncategorized" rel="category tag">Uncategorized</a>
									<span class="post-meta-devide"> | </span>
									Tag: <a href="#" rel="tag">sticky</a>, <a href="#" rel="tag">template</a>
								</div>
							</div>
							
				    </article>


				    <hr>


						<article class="blog-classic blog-timestamp-content-left">
							<div class="article-img">
								<img src="http://local.wxeo.com/WXEO/wp-content/uploads/2014/05/tumblr_n6rzwbv3ji1st5lhmo1_1280-1140x380.jpg">
								<div class="timestamp"><time datetime="2014-06-24T22:45:25+00:00" class="updated">20<span>Feb</span><!-- <span class="year">2014</span> --></time></div>
							</div>
				      
				      <div class="blog-content">
					      <h2>Blog Title Goes Here</h2>
					      <p>This is a sticky post. There are a few things to verify: The sticky post should be distinctly recognizable in some way in comparison to normal posts. You can style the .sticky class if you are using the post_class() function to generate your post classes, which is a best practice. They should…</p>
					      <div class="entry-meta">
					      	<a class="btn btn-border btn-sm read-more">Read More</a>
									<span class="byline"> by <span class="author vcard"><a class="url fn n" href="#">admin</a></span></span>
									<span class="post-meta-devide"> | </span>
									<a href="#" title="View all posts in Uncategorized" rel="category tag">Uncategorized</a>
									<span class="post-meta-devide"> | </span>
									Tag: <a href="#" rel="tag">sticky</a>, <a href="#" rel="tag">template</a>
								</div>
							</div>
							
				    </article>


				    <hr>


						<article class="blog-classic blog-border-meta">
							<div class="article-img">
								<img src="http://local.wxeo.com/WXEO/wp-content/uploads/2014/05/tumblr_n6rzwbv3ji1st5lhmo1_1280-1140x380.jpg">
							</div>
				      
				      <h2>Blog Title Goes Here</h2>
				      <p>This is a sticky post. There are a few things to verify: The sticky post should be distinctly recognizable in some way in comparison to normal posts. You can style the .sticky class if you are using the post_class() function to generate your post classes, which is a best practice. They should…</p>
				      <div class="entry-meta">
								<span class="byline"><i class="fa fa-user"></i><span class="author vcard"><a class="url fn n" href="#">admin</a></span></span>
								<span class="post-meta-devide"> | </span>
								<span class="meta meta-date"><i class="fa fa-pencil"></i><time datetime="2014-06-24T22:45:25+00:00" class="updated">February 20, 2014</time></span>
								<span class="post-meta-devide"> | </span>
								<span><i class="fa fa-comments"></i><a href="#" title="View all posts in Uncategorized" rel="category tag">Uncategorized</a></span>
								<span class="post-meta-devide"> | </span>
								<span><i class="fa fa-list"></i>Tag: <a href="#" rel="tag">sticky</a>, <a href="#" rel="tag">template</a></span>
							</div>
							
						</article>




						<hr>




						<article class="blog-classic blog-timestamp-border">
							<div class="article-img">
								<img src="http://local.wxeo.com/WXEO/wp-content/uploads/2014/05/tumblr_n6rzwbv3ji1st5lhmo1_1280-1140x380.jpg">
							</div>
				      <div class="timestamp pull-left"><time datetime="2014-06-24T22:45:25+00:00" class="updated">20<span>Feb</span><!-- <span class="year">2014</span> --></time></div>
				      <div class="blog-content">
					      <h2>Blog Title Goes Here</h2>
					      <p>This is a sticky post. There are a few things to verify: The sticky post should be distinctly recognizable in some way in comparison to normal posts. You can style the .sticky class if you are using the post_class() function to generate your post classes, which is a best practice. They should…</p>
					      <div class="entry-meta">
					      	<a class="btn btn-border btn-sm read-more">Read More</a>
									<span class="byline"> by <span class="author vcard"><a class="url fn n" href="#">admin</a></span></span>
									<span class="post-meta-devide"> | </span>
									<a href="#" title="View all posts in Uncategorized" rel="category tag">Uncategorized</a>
									<span class="post-meta-devide"> | </span>
									Tag: <a href="#" rel="tag">sticky</a>, <a href="#" rel="tag">template</a>
								</div>
							</div>
							
				    </article>




						<hr>


						<a class="btn btn-xl btn-box">Button</a>
						<a class="btn btn-xl btn-border">Button</a>
						<a class="btn btn-xl btn-bottom-line">Button</a>
						<a class="btn btn-xl btn-top-line">Button</a>
						<a class="btn btn-xl btn-box-border-top">Button</a>
						<a class="btn btn-xl btn-box-border-bottom">Button</a>
						<a class="btn btn-xl btn-box-border-left">Button</a>
						<a class="btn btn-xl btn-box-border-right">Button</a>
						<br><br>
						<a class="btn btn-lg btn-box">Button</a>
						<a class="btn btn-lg btn-border">Button</a>
						<a class="btn btn-lg btn-bottom-line">Button</a>
						<a class="btn btn-lg btn-top-line">Button</a>
						<a class="btn btn-lg btn-box-border-top">Button</a>
						<a class="btn btn-lg btn-box-border-bottom">Button</a>
						<a class="btn btn-lg btn-box-border-left">Button</a>
						<a class="btn btn-lg btn-box-border-right">Button</a>
						<br><br>
						<a class="btn btn-md btn-box">Button</a>
						<a class="btn btn-md btn-border">Button</a>
						<a class="btn btn-md btn-bottom-line">Button</a>
						<a class="btn btn-md btn-top-line">Button</a>
						<a class="btn btn-md btn-box-border-top">Button</a>
						<a class="btn btn-md btn-box-border-bottom">Button</a>
						<a class="btn btn-md btn-box-border-left">Button</a>
						<a class="btn btn-md btn-box-border-right">Button</a>
						<br><br>
						<a class="btn btn-sm btn-box">Button</a>
						<a class="btn btn-sm btn-border">Button</a>
						<a class="btn btn-sm btn-bottom-line">Button</a>
						<a class="btn btn-sm btn-top-line">Button</a>
						<a class="btn btn-sm btn-box-border-top">Button</a>
						<a class="btn btn-sm btn-box-border-bottom">Button</a>
						<a class="btn btn-sm btn-box-border-left">Button</a>
						<a class="btn btn-sm btn-box-border-right">Button</a>
						<br><br>
						<a class="btn btn-xs btn-box">Button</a>
						<a class="btn btn-xs btn-border">Button</a>
						<a class="btn btn-xs btn-bottom-line">Button</a>
						<a class="btn btn-xs btn-top-line">Button</a>
						<a class="btn btn-xs btn-box-border-top">Button</a>
						<a class="btn btn-xs btn-box-border-bottom">Button</a>
						<a class="btn btn-xs btn-box-border-left">Button</a>
						<a class="btn btn-xs btn-box-border-right">Button</a>


						<hr>
					

						
						<div class="row portfolio-shortcode">
							
							<div class="col-md-3 portfolio-item">
								<img class="full-img" src="http://placekitten.com/300/200">
								<div class="wxeo-caption">
									<h3>Image Title</h3>
									<p>Lorem Ipsum is simply dummy text of the printing and typesetting industry.</p>
									<a class="btn btn-border"><i class="fa fa-shield"></i> Button</a>
								</div>
							</div>

							<div class="col-md-3 portfolio-item">
								<img class="full-img" src="http://placekitten.com/300/200">
								<div class="wxeo-caption">
									<h3>Image Title</h3>
									<p>Lorem Ipsum is simply dummy text of the printing and typesetting industry.</p>
									<a class="btn btn-border"><i class="fa fa-search-plus"></i> Button</a>
								</div>
							</div>

							<div class="col-md-3 portfolio-item">
								<img class="full-img" src="http://placekitten.com/300/200">
								<div class="wxeo-caption">
									<h3>Image Title</h3>
									<p>Lorem Ipsum is simply dummy text of the printing and typesetting industry.</p>
									<a class="btn btn-border"><i class="fa fa-shield"></i> Button</a>
								</div>
							</div>

							<div class="col-md-3 portfolio-item">
								<img class="full-img" src="http://placekitten.com/300/200">
								<div class="wxeo-caption">
									<h3>Image Title</h3>
									<p>Lorem Ipsum is simply dummy text of the printing and typesetting industry.</p>
									<a class="btn btn-border"><i class="fa fa-shield"></i> Button</a>
								</div>
							</div>

							<div class="col-md-3 portfolio-item">
								<img class="full-img" src="http://placekitten.com/300/200">
								<div class="wxeo-caption">
									<h3>Image Title</h3>
									<p>Lorem Ipsum is simply dummy text of the printing and typesetting industry.</p>
									<a class="btn btn-border"><i class="fa fa-shield"></i> Button</a>
								</div>
							</div>

							<div class="col-md-3 portfolio-item">
								<img class="full-img" src="http://placekitten.com/300/200">
								<div class="wxeo-caption">
									<h3>Image Title</h3>
									<p>Lorem Ipsum is simply dummy text of the printing and typesetting industry.</p>
									<a class="btn btn-border"><i class="fa fa-search-plus"></i> Button</a>
								</div>
							</div>

							<div class="col-md-3 portfolio-item">
								<img class="full-img" src="http://placekitten.com/300/200">
								<div class="wxeo-caption">
									<h3>Image Title</h3>
									<p>Lorem Ipsum is simply dummy text of the printing and typesetting industry.</p>
									<a class="btn btn-border"><i class="fa fa-shield"></i> Button</a>
								</div>
							</div>

							<div class="col-md-3 portfolio-item">
								<img class="full-img" src="http://placekitten.com/300/200">
								<div class="wxeo-caption">
									<h3>Image Title</h3>
									<p>Lorem Ipsum is simply dummy text of the printing and typesetting industry.</p>
									<a class="btn btn-border"><i class="fa fa-shield"></i> Button</a>
								</div>
							</div>

						</div>





					<?php endwhile; // end of the loop. ?>
				</main>

			
				<?php if ( comments_open() || '0' != get_comments_number() ) { comments_template(); } // Page Comments ?>

			</div>
		</div>

	</div><!-- .container -->



<?php get_footer(); ?>
